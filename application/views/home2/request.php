<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="<?=base_url();?>assets/home/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Bank Sampah</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="<?=base_url();?>assets/home/css/bootstrap.css" rel="stylesheet" />
        <link href="<?=base_url();?>assets/home/css/landing-page.css" rel="stylesheet"/>

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="<?=base_url();?>assets/home/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <div class="section section-demo">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <center><h4 class="header-text">Minta Request</h4></center><br>
                    <hr>
                   <div style="width:800px; margin:0 auto;">
                        <form action="<?= base_url();?>index.php/welcome/aksi_request" method="post">
                        
                        <label for="">Jenis Request</label><br>
                        <select class="form-control border-input" required name="jenis_request" id="">
                                <option value="">-- Pilih Jenis Request --</option>
                                <option value="penukaran sampah">Penukaran Sampah</option>
                                <option value="penukaran uang">Penukaran Uang</option>
                                <option value="sumbang sampah">Sumbang Sampah</option>
                        </select><br>
                        
                        <CENTER><input type="submit" class="btn btn-fill btn-success" value="Minta Request"></CENTER>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>