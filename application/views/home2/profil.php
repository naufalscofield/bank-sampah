<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="<?=base_url();?>assets/home/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Bank Sampah</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="<?=base_url();?>assets/home/css/bootstrap.css" rel="stylesheet" />
        <link href="<?=base_url();?>assets/home/css/landing-page.css" rel="stylesheet"/>

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="<?=base_url();?>assets/home/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <body class="landing-page landing-page1">
            <div class="section section-features">
                <div class="container">
                    <h4 class="header-text text-center">Profil Anda</h4>
                    <div class="item active">
                        <div class="mask">
                            <center><img src="<?=base_url();?>assets/home/img/new_logo.png">
                        </div>
                        <div class="carousel-testimonial-caption">
                            <center><h2><?= $this->session->userdata('nama_lengkap');?></h2></center>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-3">
                            <div class="card card-green">
                                <div class="icon">
                                    <i class="fa fa-money"></i>
                                </div>
                                <h4>Saldo</h4>
                                <?php foreach ($saldo as $saldo) { ?>
                                    <br><b><h5>Rp.<?= number_format($saldo['saldo'],2,',','.'); ?></h5></b> 
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-green">
                                <div class="icon">
                                    <i class="pe-7s-note2"></i>
                                </div>
                                <div class="text">
                                    <h4>Data Anda</h4><br>
                                    <form action="<?=base_url();?>index.php/welcome/update_profil" method="post">
                                    <br><label for="">No Rekening</label>
                                    <input value="<?= $this->session->userdata('no_rekening');?>" disabled class="form-control border-input">
                                    <br><label for="">Nama Lengkap</label><br>
                                    <input type="text" class="form-control border-input" required name="nama_lengkap" value="<?= $this->session->userdata('nama_lengkap');?>"><BR>

                                    <label for="">Alamat</label><br>
                                    <textarea required name="alamat" id="" cols="30" rows="10" class="form-control border-input"><?= $this->session->userdata('alamat');?></textarea><br>
                                    
                                    <label for="">Sektor</label><br>
                                    <select class="form-control border-input" required name="id_sektor" id="">
                                            <option value="">-- Pilih Sektor --</option>
                                            <?php foreach ($sektor as $sektor) { ?>
                                            <option value="<?= $sektor['id'];?>"><?= $sektor['sektor'];?></option>
                                            <?php } ?>
                                    </select><br>
                                    
                                    <label for="">Username</label><br>
                                    <input type="text" class="form-control border-input" required name="username" value="<?= $this->session->userdata('username');?>"><br>
                                    
                                    <label for="">Password</label><br>
                                    <input type="password" class="form-control border-input" required name="password" value="<?= $this->session->userdata('pw');?>"><br>
                                    
                                    <label for="">No Telp</label><br>
                                    <input type="number" class="form-control border-input" required name="no_telp" value="<?= $this->session->userdata('no_telp');?>"><br>
                                    
                                    <label for="">Jenis Nasabah</label><br>
                                    <select class="form-control border-input" required name="jenis_nasabah" id="">
                                            <option value="">-- Pilih Jenis Nasabah --</option>
                                            <option value="individual">Individual</option>
                                            <option value="organisasi">Organisasi</option>
                                            <option value="penyumbang">Penyumbang</option>
                                    </select><br>
                                    <CENTER><input type="submit" class="btn btn-fill btn-success" value="UPDATE"></CENTER>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card card-green">
                                <div class="icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <h4>Point</h4>
                                <?php foreach ($point as $point) { ?>
                                    <h2><?=$point['point']; ?></h2> 
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
           
