<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="<?=base_url();?>assets/home/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Bank Sampah</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="<?=base_url();?>assets/home/css/bootstrap.css" rel="stylesheet" />
        <link href="<?=base_url();?>assets/home/css/landing-page.css" rel="stylesheet"/>

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="<?=base_url();?>assets/home/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <div class="section section-demo">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <center><h4 class="header-text">Daftar Nasabah</h4></center><br>
                    <hr>
                   <div style="width:800px; margin:0 auto;">
                        <form action="<?= base_url();?>index.php/welcome/aksi_daftar" method="post">

                        <label for="">Nama Lengkap</label><br>
                        <input type="text" class="form-control border-input" required name="nama_lengkap"><BR>

                        <label for="">Alamat</label><br>
                        <textarea required name="alamat" id="" cols="30" rows="10" class="form-control border-input"></textarea><br>
                        
                        <label for="">Sektor</label><br>
                        <select class="form-control border-input" required name="id_sektor" id="">
                                <option value="">-- Pilih Sektor --</option>
                                <?php foreach ($sektor as $sektor) { ?>
                                <option value="<?= $sektor['id'];?>"><?= $sektor['sektor'];?></option>
                                <?php } ?>
                        </select><br>
                        
                        <label for="">Username</label><br>
                        <input type="text" class="form-control border-input" required name="username"><br>
                        
                        <label for="">Password</label><br>
                        <input type="password" class="form-control border-input" required name="password"><br>
                        
                        <label for="">No Telp</label><br>
                        <input type="number" class="form-control border-input" required name="no_telp"><br>
                        
                        <label for="">Jenis Nasabah</label><br>
                        <select class="form-control border-input" required name="jenis_nasabah" id="">
                                <option value="">-- Pilih Jenis Nasabah --</option>
                                <option value="individual">Individual</option>
                                <option value="organisasi">Organisasi</option>
                                <option value="penyumbang">Penyumbang</option>
                        </select><br>
                        
                        <CENTER><input type="submit" class="btn btn-fill btn-success" value="DAFTAR"></CENTER>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>