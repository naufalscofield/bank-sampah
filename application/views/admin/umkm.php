<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">UMKM</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#addUmkm"><i class="fa fa-plus"></i></button></h4>
                            </div>
                            <div class="content table-responsive">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama UMKM</b></center></th>
                                            <th width="20"><center><b>Alamat</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($umkm as $umkm) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $umkm['nama_umkm']; ?></b></center></td>
                                            <td width="20"><center><b><?= $umkm['alamat']; ?></b></center></td>
                                            <td width="20"><center>
                                            <a href='<?= base_url();?>index.php/admin/deleteUmkm/<?= $umkm['id'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                                            <a id='btn_edit' data-id_edit='<?= $umkm['id']; ?>' class='btn btn-info' data-toggle='modal' data-target='#editUmkm'><i class='fa fa-pencil'></i></a>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addUmkm" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Tambah UMKM</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>index.php/admin/insertUmkm" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama UMKM</label>
                                        <input type="text" required id="nama_umkm" class="form-control border-input" placeholder="Nama UMKM" value="" name="nama_umkm">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <input type="text" required id="alamat" class="form-control border-input" placeholder="Alamat" value="" name="alamat">
                                    </div>
                                </div>
                            </div>
                        
                            <div class="clearfix"></div>
                                <div class="modal-footer">
                                    <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                        Tambah UMKM
                                    </button>
                                    <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            <div id="editUmkm" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Edit UMKM</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>index.php/admin/updateUmkm">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">

                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_nama_umkm">
                                        
                                    </div>
                                </div>
                        </div>

                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_alamat">
                                        
                                    </div>
                                </div>
                            </div>  

                        <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Edit UMKM
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <hr>
    </div>
</div>

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        // dom: 'Bfrtip',
        dom: 'lBfrtip',
        buttons: [
    'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print'
  ]
    } );

    $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("id_edit");
                console.log(id)
                $.get("http://localhost/banksampah/index.php/admin/showUmkm/" + id, function(data, status){
                console.log(status)
                data = JSON.parse(data);
                console.log(data)
                if (status) {
                    data.forEach(element => {
                    $('#input_id').html("")
                        $('#input_id').append("<input type='hidden' required id='id_edit' class='form-control border-input' name='id_edit' value="+element.id+">");
                    $('#input_nama_umkm').html("")
                        $('#input_nama_umkm').append("<label>Nama UMKM</label><input type='text' required id='nama_umkm_edit' class='form-control border-input' name='nama_umkm_edit' value='"+element.nama_umkm+"'>");
                    $('#input_alamat').html("")
                        $('#input_alamat').append("<label>Alamat</label><input type='text' required id='alamat_edit' class='form-control border-input' name='alamat_edit' value='"+element.alamat+"'>");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });
            });  

} );
</script>