<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet" />

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/af-2.1.2/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.js"></script> -->


<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Barang</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#addBarang"><i class="fa fa-plus"></i></button></h4>
                            </div>
                            <div class="content table-responsive">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Barang</b></center></th>
                                            <th width="20"><center><b>Kategori</b></center></th>
                                            <!-- <th width="20"><center><b>Deskripsi</b></center></th> -->
                                            <th width="20"><center><b>Harga / Poin</b></center></th>
                                            <th width="20"><center><b>Stok</b></center></th>
                                            <th width="20"><center><b>Foto</b></center></th>
                                            <th width="20"><center><b>Jenis</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($barang as $barang) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $barang['nama_barang']; ?></b></center></td>
                                            <td width="20"><center><b><?= $barang['nama_kategori']; ?></b></center></td>
                                            <?php if ($barang['jenis'] == 'regular'){?>
                                            <td width="20"><center><b>Rp.<?= number_format($barang['harga'],2,',','.'); ?></b></center></td>
                                            <?php } else { ?>
                                            <td width="20"><center><b><?= $barang['poin']; ?> Poin</b></center></td>
                                            <?php } ?>
                                            <td width="20"><center><b><?= $barang['stok']; ?></b></center></td>
                                            <td width="20"><center><img style = "width:100px;height:100px" src="<?= base_url();?>assets/barang/<?= $barang['foto'];?>" alt=""></b></center></td>
                                            <td width="20"><center><b><?= $barang['jenis']; ?></b></center></td>
                                            <td width="20"><center>
                                            <a href='<?= base_url();?>index.php/admin/deleteBarang/<?= $barang['id'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                                            <a id='btn_edit' data-id_edit='<?= $barang['id']; ?>' class='btn btn-info' data-toggle='modal' data-target='#editBarang'><i class='fa fa-pencil'></i></a>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addBarang" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Tambah Barang</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>index.php/admin/insertBarang" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input type="text" required id="nama_barang" class="form-control border-input" placeholder="Nama Barang" value="" name="nama_barang">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kategori</label>
                                        <select name="id_kategori" required class="form-control border-input" id="id_kategori">
                                            <option value="">-- Pilih Kategori --</option>
                                            <?php
                                                foreach ($kategori as $kategori) { ?>
                                            <option value="<?= $kategori['id'];?>"><?= $kategori['nama_kategori'];?></option>
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis</label>
                                        <select name="jenis" required class="form-control border-input" id="jenis">
                                            <option value="">-- Pilih Jenis Barang --</option>                                  
                                            <option value="regular">Regular</option>                                  
                                            <option value="eksklusif">Eksklusif</option>                                  
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Stok</label>
                                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id="stok" class="form-control border-input" placeholder="No Telp" value="" name="stok">
                                    </div>
                                </div>
                            </div>

        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Harga</label>
                                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id="harga" class="form-control border-input" placeholder="No Telp" value="0" name="harga">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Poin</label>
                                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id="poin" class="form-control border-input" placeholder="No Telp" value="0" name="poin">
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Foto</label>
                                        <input type="file" class="form-control border-input" name="foto" id="foto" required>
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Deskripsi</label>
                                        <textarea name="deskripsi" id="deskripsi" class="form-control border-input" required cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>    
                        
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                    Tambah Barang
                                </button>
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="editBarang" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Edit Barang</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>index.php/admin/updateBarang" enctype="multipart/form-data">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">

                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_nama_barang">
                                        
                                    </div>
                                </div> 
                       
                                <div class="col-md-6">
                                    <div class="form-group" id="input_id_kategori">
                                    <label>Kategori</label>
                                        <select name="id_kategori_edit" required class="form-control border-input" id="id_kategori_edit">
                                        <option value="">-- Pilih Kategori --</option>
                                            <?php
                                                foreach ($kategori_edit as $kategori) { ?>
                                            <option value="<?= $kategori['id'];?>"><?= $kategori['nama_kategori'];?></option>
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>  
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis</label>
                                        <select name="jenis_edit" required class="form-control border-input" id="jenis_edit">
                                            <option value="">-- Pilih Jenis Barang --</option>                                  
                                            <option value="regular">Regular</option>                                  
                                            <option value="eksklusif">Eksklusif</option>                                  
                                        </select>
                                    </div>
                                </div>

                            <div class="col-md-6">
                                    <div class="form-group" id="input_stok">
                                       
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_harga">
                                        
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group" id="input_poin">
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" >
                                        <label for="">Foto</label>
                                        <input type="file" class="form-control border-input" name="input_foto" id="input_foto">
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_deskripsi">
                                        
                                    </div>
                                </div>
                            </div>    

                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Edit Barang
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>
    </div>
</div>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        // dom: 'Bfrtip',
        dom: 'lBfrtip',
        buttons: [
    'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print'
  ]
    } );

    $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("id_edit");
                console.log(id)
                $.get("http://localhost/banksampah/index.php/admin/showBarang/" + id, function(data, status){
                console.log(data, status)
                data = JSON.parse(data);
                if (status) {
                    data.forEach(element => {
                    $('#input_id').html("")
                        $('#input_id').append("<input type='hidden' required id='id_edit' class='form-control border-input' name='id_edit' value="+element.id+">");
                    $('#input_nama_barang').html("")
                        $('#input_nama_barang').append("<label>Nama Barang</label><input type='text' required id='nama_barang_edit' class='form-control border-input' name='nama_barang_edit' value='"+element.nama_barang+"'>");
                        if (element.jenis == 'regular') {
                    $('#input_harga').html("")
                        $('#input_harga').append("<label>Harga</label><input type='text' required id='harga_edit' class='form-control border-input' name='harga_edit' value="+element.harga+">");
                    $('#input_poin').html("")
                        $('#input_poin').append("<label>Poin</label><input type='text' disabled required id='poin_edit' class='form-control border-input' name='poin_edit' value="+element.poin+">");
                        } else { 
                    $('#input_poin').html("")
                        $('#input_poin').append("<label>Poin</label><input type='text' required id='poin_edit' class='form-control border-input' name='poin_edit' value="+element.poin+">");
                    $('#input_harga').html("")
                        $('#input_harga').append("<label>Harga</label><input type='text' disabled required id='harga_edit' class='form-control border-input' name='harga_edit' value="+element.harga+">");
                        }
                    $('#input_stok').html("")
                        $('#input_stok').append("<label>Stok</label><input type='text' required id='stok_edit' class='form-control border-input' name='stok_edit' value="+element.stok+">");
                    $('#input_deskripsi').html("")
                        $('#input_deskripsi').append("<label for=''>Deskripsi</label><textarea name='deskripsi_edit' id='deskripsi_edit' class='form-control border-input' required cols='30' rows='10'>"+element.deskripsi+"</textarea>");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });
            });  
   
    $(document).on('change', '#jenis' ,function(){
               var jenis = $('#jenis').val()
            //    console.log(jenis)
            if (jenis == 'regular'){
                $("#poin").prop('disabled', true);
                $("#harga").prop('disabled', false);
            } else {
                $("#harga").prop('disabled', true);
                $("#poin").prop('disabled', false);
            }
        });  
   
    $(document).on('change', '#jenis_edit' ,function(){
               var jenis = $('#jenis_edit').val()
            //    console.log(jenis)
            if (jenis == 'regular'){
                $("#poin_edit").prop('disabled', true);
                $("#poin_edit").val(0);
                $("#harga_edit").prop('disabled', false);
            } else {
                $("#harga_edit").prop('disabled', true);
                $("#harga_edit").val(0);
                $("#poin_edit").prop('disabled', false);
            }
        });  

} );
</script>