<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Sumbang Sampah</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Nasabah</b></center></th>
                                            <th width="20"><center><b>Tanggal</b></center></th>
                                            <th width="20"><center><b>Jenis Sampah</b></center></th>
                                            <th width="20"><center><b>Berat / QTY</b></center></th>
                                            <th width="20"><center><b>Satuan</b></center></th>
                                            <!-- <th width="20"><center><b>Keterangan</b></center></th> -->
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($transaksi as $transaksi) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['nama_lengkap']; ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['tanggal']; ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['jenis_sampah']; ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['berat']; ?></b></center></td>
                                            <td width="20"><center><b><?= $transaksi['satuan']; ?></b></center></td>
                                            <!-- <td width="20"><center><b><?= $transaksi['keterangan']; ?></b></center></td> -->
                                            
                                            <td width="20"><center>
                                            <!-- <a href='<?= base_url();?>index.php/admin/deleteSumbangSampah/<?= $transaksi['id_sumbang_sampah_detail'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a> -->
                                            <a id='btn_edit' data-id_edit='<?= $transaksi['id_sumbang_sampah_detail']; ?>' data-nama='<?= $transaksi['nama_lengkap']; ?>' data-tanggal='<?= $transaksi['tanggal']; ?>' class='btn btn-info' data-toggle='modal' data-target='#hibahSampah'>Hibahkan<i class='fa fa-pencil'></i></a>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            <div id="hibahSampah" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Hibah Sampah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="" method="post" action="<?= base_url();?>index.php/Admin/AksiHibahSampah">
                        <input type="hidden" id="id_sumbang_sampah_detail" name="id_sumbang_sampah_detail" value="">
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="id_sumbang_sampah">

                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="jenis_sampah">
                                   
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="berat">
                                    <!-- <input type="text" readonly id="" name="harga" class="form-control border-input"> -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="satuan">
                                    <!-- <input type="text" readonly id="" name="harga" class="form-control border-input"> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="id_umkm">
                                    <select class="form-control border-input" name="id_umkm" id="">
                                    <?php $umkm = $this->db->get('tb_umkm')->result_array();
                                    foreach ($umkm as $umkm) { ?>
                                        <option value="<?= $umkm['id'];?>"><?= $umkm['nama_umkm'];?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>  
                        </div>  

                        <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Hibahkan
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        // dom: 'Bfrtip',
        dom: 'lBfrtip',
        buttons: [
    'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print'
  ]
    } );

    // $(document).on 

    $(document).on('click', '#btn_edit' ,function(){
        $isd = $(this).data("id_edit");
        $('#id_sumbang_sampah_detail').val($isd)
        $.ajax({
            type: "POST",
            data: { id_sumbang_sampah_detail: $(this).data("id_edit") },
            url: '<?php echo base_url()."index.php/admin/showHibahSampah" ?>',
            dataType: 'text',
            success: function(resp) {
            var json = JSON.parse(resp.replace(',.', ''))
            var $i = $("#berat");
            var $h = $("#berat");
            var $s = $("#satuan");
            var $j = $("#satuan");
            $h.empty(); // remove old options
            $s.empty(); // remove old options
            // $h.append($("<option></option>")
            // .attr("value", '').text('-- Pilih Matpel --'));
            $.each(json, function(key, value) {
                $i.append($("<label for='' id=''>Id Sumbang Sampah</label>"))
                $i.append($("<input type='text' id='id_sumbang_sampah' readonly name='id_sumbang_sampah' class='form-control border-input'>")
                .attr("value", value.id_sumbang_sampah));
                });
            $.each(json, function(key, value) {
                $h.append($("<label for='' id=''>Berat</label>"))
                $h.append($("<input type='number' required id='berat' max="+value.berat+" name='berat' class='form-control border-input'>")
                .attr("value", value.berat));
                });
            $.each(json, function(key, value) {
                $s.append($("<label for='' id=''>Satuan</label>"))
                $s.append($("<input type='text' id='satuan' readonly name='satuan' class='form-control border-input'>")
                .attr("value", value.satuan));
                });
            $.each(json, function(key, value) {
                $j.append($("<label for='' id=''>Jenis Sampah</label>"))
                $j.append($("<input type='text' id='jenis_sampah' readonly name='jenis_sampah' class='form-control border-input'>")
                .attr("value", value.jenis_sampah));
                });
           
            },
            error: function (jqXHR, exception) {
            console.log(jqXHR, exception)
            }
        });
            });  

} );
</script>