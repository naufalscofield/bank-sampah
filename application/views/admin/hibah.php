<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Hibah Sampah</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive">
                            <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Perusahaan</b></center></th>
                                            <th width="20"><center><b>Nama UMKM</b></center></th>
                                            <th width="20"><center><b>Jenis Sampah</b></center></th>
                                            <th width="20"><center><b>Berat / QTY</b></center></th>
                                            <th width="20"><center><b>Satuan</b></center></th>
                                            <!-- <th width="20"><center><b>Keterangan</b></center></th> -->
                                            <!-- <th width="20"><center><b>Aksi</b></center></th> -->
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($hibah as $hibah) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $hibah['nama_lengkap']; ?></b></center></td>
                                            <td width="20"><center><b><?= $hibah['nama_umkm']; ?></b></center></td>
                                            <td width="20"><center><b><?= $hibah['jenis_sampah']; ?></b></center></td>
                                            <td width="20"><center><b><?= $hibah['berat']; ?></b></center></td>
                                            <td width="20"><center><b><?= $hibah['satuan']; ?></b></center></td>
                                            <!-- <td width="20"><center><b><?= $hibah['keterangan']; ?></b></center></td> -->
                                            
                                            <td width="20"><center>
                                            <!-- <a href='<?= base_url();?>index.php/admin/deleteHibahSampah/<?= $hibah['id_sumbang_sampah_detail'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a> -->
                                            <!-- <a id='btn_edit' data-id_edit='<?= $hibah['id_sumbang_sampah_detail']; ?>' data-nama='<?= $hibah['nama_lengkap']; ?>' data-tanggal='<?= $hibah['tanggal']; ?>' class='btn btn-info' data-toggle='modal' data-target='#hibahSampah'>Hibahkan<i class='fa fa-pencil'></i></a> -->
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        // dom: 'Bfrtip',
        dom: 'lBfrtip',
        buttons: [
    'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print'
  ]
    } );

    $(document).on('click', '#btn_edit' ,function(){
        $.ajax({
            type: "POST",
            data: { id_sumbang_sampah_detail: $(this).data("id_edit") },
            url: '<?php echo base_url()."index.php/admin/showHibahSampah" ?>',
            dataType: 'text',
            success: function(resp) {
            var json = JSON.parse(resp.replace(',.', ''))
            var $i = $("#berat");
            var $h = $("#berat");
            var $s = $("#satuan");
            var $j = $("#satuan");
            $h.empty(); // remove old options
            $s.empty(); // remove old options
            // $h.append($("<option></option>")
            // .attr("value", '').text('-- Pilih Matpel --'));
            $.each(json, function(key, value) {
                $i.append($("<label for='' id=''>Id Sumbang Sampah</label>"))
                $i.append($("<input type='text' id='id_sumbang_sampah' readonly name='id_sumbang_sampah' class='form-control border-input'>")
                .attr("value", value.id_sumbang_sampah));
                });
            $.each(json, function(key, value) {
                $h.append($("<label for='' id=''>Berat</label>"))
                $h.append($("<input type='text' id='berat'  name='berat' class='form-control border-input'>")
                .attr("value", value.berat));
                });
            $.each(json, function(key, value) {
                $s.append($("<label for='' id=''>Satuan</label>"))
                $s.append($("<input type='text' id='satuan' readonly name='satuan' class='form-control border-input'>")
                .attr("value", value.satuan));
                });
            $.each(json, function(key, value) {
                $j.append($("<label for='' id=''>Jenis Sampah</label>"))
                $j.append($("<input type='text' id='jenis_sampah' readonly name='jenis_sampah' class='form-control border-input'>")
                .attr("value", value.jenis_sampah));
                });
           
            },
            error: function (jqXHR, exception) {
            console.log(jqXHR, exception)
            }
        });
            });  

} );
</script>