<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Penjualan</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#addJual"><i class="fa fa-plus"></i>Jual Sampah</button></h4>
                            </div>
                            <div class="content table-responsive">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Pembeli</b></center></th>
                                            <th width="20"><center><b>Jenis Sampah</b></center></th>
                                            <th width="20"><center><b>Berat</b></center></th>
                                            <th width="20"><center><b>Total</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($pp as $pp) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $pp['pembeli']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pp['jenis_sampah']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pp['berat']; ?></b></center></td>
                                            <td width="20"><center><b>Rp.<?= number_format($pp['total'],2,',','.'); ?></b></center></td>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addJual" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Jual Sampah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>index.php/admin/insertPenjualan" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Dijual Kepada</label>
                                        <input type="text" required id="pembeli" class="form-control border-input" placeholder="" value="" name="pembeli">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jenis Sampah</label>
                                        <?php
                                        $jenis = $this->db->get('tb_jenis_sampah')->result_array();
                                        ?>
                                        <select required class="form-control border-input" name="id_jenis_sampah" id="id_jenis_sampah">
                                            <option value="">-- Pilih Jenis Sampah --</option>
                                            <?php foreach ($jenis as $jenis) { ?>
                                            <option value="<?=$jenis['id'];?>"><?=$jenis['jenis_sampah'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="stok">
                                    
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="harga">
                                    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" id="berat">

                                    </div>
                                </div>
                            </div>
                            
                        
                            <div class="clearfix"></div>
                                <div class="modal-footer">
                                    <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                        Input Penjualan
                                    </button>
                                    <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

        <hr>
    </div>
</div>

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        // dom: 'Bfrtip',
        dom: 'lBfrtip',
        buttons: [
    'excelHtml5', 'pdfHtml5', 'csvHtml5', 'print'
  ]
    } );

$('#id_jenis_sampah').on('change', function() {

    $.ajax({
        type: "POST",
        data: { id_jenis_sampah: $('#id_jenis_sampah').val() },
        url: '<?php echo base_url()."index.php/admin/getHargaSampah" ?>',
        dataType: 'text',
        success: function(resp) {
        var json = JSON.parse(resp.replace(',.', ''))
        var $h = $("#harga");
        var $b = $("#berat");
        $h.empty(); // remove old options
        $b.empty(); // remove old options
        $.each(json, function(key, value) {
            var bati = value.harga * 0.10
            var harga = parseInt(value.harga) + parseInt(bati)
            $h.append($("<label for='' id=''>Harga</label>"))
            $h.append($("<input type='text' id='harga' readonly name='harga' class='form-control border-input'>")
            .attr("value", harga));
            });
        $.each(json, function(key, value) {
            $b.append($("<label for='' id=''>Berat</label>"))
            $b.append($("<input type='number' id='berat_input' name='berat_input' required class='form-control border-input'>")
            );
            });
        },
        error: function (jqXHR, exception) {
        console.log(jqXHR, exception)
        }
    });
    
    $.ajax({
        type: "POST",
        data: { id_jenis_sampah: $('#id_jenis_sampah').val() },
        url: '<?php echo base_url()."index.php/admin/getStokTersedia" ?>',
        dataType: 'text',
        success: function(resp) {
        var json = JSON.parse(resp.replace(',.', ''))
        var $s = $("#stok");
        $s.empty(); // remove old options
            $s.append($("<label for='' id=''>Stok Tersedia</label>"))
            $s.append($("<input type='text' id='stok_input' readonly name='stok' class='form-control border-input'>")
            .attr("value", json.stok));
            // });
        },
    });
}); 

    $('#berat_input').on('input', function () {
    
        var value = $(this).val();
        var max = $("#stok_input")

        console.log(max)

        if ((value !== '') && (value.indexOf('.') === -1)) {
            
            $(this).val(Math.max(Math.min(value, max), -max));
        }
    });


} );
</script>