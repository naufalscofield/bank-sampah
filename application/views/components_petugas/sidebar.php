<?php
    $uri = $this->uri->segment(2);
?>
<div class="sidebar" data-background-color="white" data-active-color="danger">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="<?= base_url();?>assets/img/logo2.png" style="width:150px;height:70px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
            <?php
                if ($uri == 'getProfil') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/petugas/getProfil">
                        <i class="fa fa-user-circle"></i>
                        <p>Profil</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getRequest') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/petugas/getRequest">
                        <i class="fa fa-inbox"></i>
                        <?php 
                        $q = $this->db->get_where('tb_request',array('status' => 'diproses', 'jenis_request' => 'penukaran sampah'));
                        $notif = $q->num_rows();
                        ?>
                        <p>Minta Tukar Sampah <span class="badge"><?= $notif;?></span></p>
                    </a>
                </li>
           
            <?php
                if ($uri == 'getRequestSumbang') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/petugas/getRequestSumbang">
                        <i class="fa fa-inbox"></i>
                        <?php 
                        $q = $this->db->get_where('tb_request',array('status' => 'diproses','jenis_request' => 'sumbang sampah'));
                        $notif = $q->num_rows();
                        ?>
                        <p>Minta Sumbang Sampah <span class="badge"><?= $notif;?></span></p>
                    </a>
                </li>

            <!-- <?php
                if ($uri == 'getTransaksiSampah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/petugas/getTransaksiSampah">
                        <i class="fa fa-trash"></i>
                        <p>Transaksi Sampah</p>
                    </a>
                </li> -->
                
            </ul>
    	</div>
    </div>