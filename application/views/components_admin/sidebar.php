<style>
.circle {
  width: 2px;
  height: 2px;
  border-radius: 50%;
  font-size: 50px;
  color: black;
  line-height: 500px;
  text-align: center;
  background: red
}
</style>
<?php
    $uri = $this->uri->segment(2);
?>
<div class="sidebar" data-background-color="white" data-active-color="danger">
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="<?= base_url();?>assets/img/logo2.png" style="width:250x;height:110px;" alt=""></center>
                </a>
            </div>

            <ul class="nav">
            <?php
                if ($uri == 'getProfil') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getProfil">
                        <i class="fa fa-user-circle"></i>
                        <p>Profil</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getPegawai') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getPegawai">
                        <i class="fa fa-id-badge"></i>
                        <p>Pegawai</p>
                    </a>
                </li>
    
            <?php
                if ($uri == 'getBarang') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>    
                    <a href="<?= base_url();?>index.php/admin/getBarang">
                        <i class="fa fa-cubes"></i>
                        <p>Barang</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getKategori') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getKategori">
                        <i class="fa fa-th-list"></i>
                        <p>Kategori Barang</p>
                    </a>
                </li>
          
            <?php
                if ($uri == 'getJenisSampah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getJenisSampah">
                        <i class="fa fa-trash-o"></i>
                        <p>Jenis Sampah</p>
                    </a>
                </li>
          
            <?php
                if ($uri == 'getNasabah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getNasabah">
                        <i class="fa fa-users"></i>
                        <?php 
                        $q = $this->db->get_where('tb_nasabah',array('status' => 'pending'));
                        $notif = $q->num_rows();
                        ?>
                        <p>Nasabah <span class="badge"><?= $notif;?></span>
                    </a>
                </li>

            <?php
                if ($uri == 'getUmkm') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getUmkm">
                        <i class="fa fa-home"></i>
                        <p>UMKM</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getRequest') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getRequest">
                        <i class="fa fa-inbox"></i>
                        <?php 
                        $q = $this->db->get_where('tb_request',array('status' => 'pending'));
                        $notif = $q->num_rows();
                        ?>
                        <p>Permintaan Panggilan <span class="badge"><?= $notif;?></span>
                        </p>
                    </a>
                </li>

            <?php
                if ($uri == 'getTransaksiSampah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getTransaksiSampah">
                        <i class="fa fa-trash"></i>
                        <p>Transaksi Sampah</p>
                    </a>
                </li>

            <?php
                if ($uri == 'getTransaksiBarang') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getTransaksiBarang">
                        <i class="fa fa-shopping-basket"></i>
                        <p>Transaksi Barang</p>
                    </a>
                </li>
           
            <?php
                if ($uri == 'getSumbangSampah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getSumbangSampah">
                        <i class="fa fa-handshake-o"></i>
                        <p>Sumbang Sampah</p>
                    </a>
                </li>
            
            <?php
                if ($uri == 'getHibah') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getHibah">
                        <i class="fa fa-th-list"></i>
                        <p>Hibah Sampah</p>
                    </a>
                </li>
            
            <?php
                if ($uri == 'getPendapatan') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getPendapatan">
                        <i class="fa fa-money"></i>
                        <p>Pendapatan Bank</p>
                    </a>
                </li>
            
            <?php
                if ($uri == 'getPenjualan') { ?>
                    <li class="active">
                <?php } else { ?>
                    <li>
                <?php } ?>
                    <a href="<?= base_url();?>index.php/admin/getPenjualan">
                        <i class="fa fa-exchange"></i>
                        <p>Penjualan Sampah</p>
                    </a>
                </li>
                
            </ul>
    	</div>
    </div>