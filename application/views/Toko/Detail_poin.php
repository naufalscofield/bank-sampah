<!-- Start Banner Area -->
<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				
						<!-- single-slide -->
						<div class="row single-slide">
							<div class="col-lg-5">
								<div class="banner-content">
									<h1>Detail Barang</h1>
									<div class="add-bag d-flex align-items-center">
										<!-- <a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase">Add to Bag</span> -->
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="teflon-banner">
									<!-- <img class="img-fluid" src="<?= base_url();?>assets/toko/img/banner/teflon-banner.png" alt=""> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!--================Single Product Area =================-->
<?php foreach ($barang as $barang) { ?>
	<div class="product_image_area">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-6">
					<div class="single-product">
						<div class="single-prd-item">
							<img class="img-fluid" src="<?= base_url();?>assets/barang/<?= $barang['foto'];?>" alt="">
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<div class="s_product_text">
						<h3><?= $barang['nama_barang'];?></h3>
						<h2><?= $barang['poin'];?> Poin</h2>
						<?php 
						$id = $this->session->userdata('id');
						$poin['poin'] = $this->db->get_where('tb_nasabah',array('id' => $id))->row();
						$pN = $poin['poin']->point;
						// print_r($poin); die;
						foreach ($poin as $poin) { ?>
						<h4>Poin anda sekarang <?= $poin->point;?> Poin</h2>
						<?php } ?>
						<ul class="list">
							<li><a class="active" href="#"><span>Ketegori</span> : <?= $barang['nama_kategori'];?></a></li>
							<li><a href="#"><span>Stok</span> : <?= $barang['stok'];?></a></li>
						</ul>
						<p><?= $barang['deskripsi'];?></p>
						<div class="product_count">
                        <form action="<?=base_url();?>index.php/toko/AksiTukarPoin" method="post">
                            <input type="hidden" name="id_barang" value="<?= $barang['id_barang'];?>">
                            <input type="hidden" name="poin" value="<?= $barang['poin'];?>">
                            <input type="hidden" name="stok" value="<?= $barang['stok'];?>">
                        </div>
						<div class="card_area d-flex align-items-center">
                        <?php if($barang['poin'] <= $pN){ ?>
                            <button type="submit" class="primary-btn">Tukar Poin Saya Dengan Barang Ini</button>
                        <?PHP } else { ?>
                            <button DISABLED class="btn btn-fill btn-danger">MAAF POIN ANDA TIDAK MENCUKUPI</button>
                        <?php } ?>
                            </form>
                            <!-- <a class="primary-btn" href="#">Tambah Ke Keranjang</a> <br> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<!--================End Single Product Area =================-->