<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="CodePixar">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Bank Sampah</title>
	<!--
		CSS
		============================================= -->
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/linearicons.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/themify-icons.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/bootstrap.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/owl.carousel.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/nice-select.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/nouislider.min.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/ion.rangeSlider.css" />
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/ion.rangeSlider.skinFlat.css" />
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/magnific-popup.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/toko/css/main.css">
</head>

<body>

	<!-- Start Header Area -->
	<header class="header_area sticky-header">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light main_box">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href="index.html"><img style="height:70px;width:150px" src="<?= base_url();?>assets/img/logo2.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav ml-auto">
                            <?php if (!$this->uri->segment(2)){ ?>
                            <li class="nav-item active"><a class="nav-link" href="<?=base_url();?>index.php/toko">Toko</a></li>
                            <?php } else { ?>
                            <li class="nav-item"><a class="nav-link" href="<?=base_url();?>index.php/toko">Toko</a></li>
                            <?php } ?>
                            
                            <?php if ($this->uri->segment(2) == 'keranjang'){ ?>
                            <li class="nav-item active"><a class="nav-link" href="<?=base_url();?>index.php/toko/keranjang">Keranjang</a></li>
                            <?php } else { ?>
                            <li class="nav-item"><a class="nav-link" href="<?=base_url();?>index.php/toko/keranjang">Keranjang</a></li>
                            <?php } ?>

                            <?php if ($this->uri->segment(2) == 'poin'){ ?>
                            <li class="nav-item active"><a class="nav-link" href="<?=base_url();?>index.php/toko/poin">Tukar Poin</a></li>
                            <?php } else { ?>
                            <li class="nav-item"><a class="nav-link" href="<?=base_url();?>index.php/toko/poin">Tukar Poin</a></li>
                            <?php } ?>

                            <?php if ($this->uri->segment(2) == 'riwayatBelanja'){ ?>
                            <li class="nav-item active"><a class="nav-link" href="<?=base_url();?>index.php/toko/riwayatBelanja">Riwayat Belanja</a></li>
                            <?php } else { ?>
                            <li class="nav-item"><a class="nav-link" href="<?=base_url();?>index.php/toko/riwayatBelanja">Riwayat Belanja</a></li>
                            <?php } ?>
                            
							<?php if ($this->uri->segment(2) == 'riwayatTukarPoin'){ ?>
                            <li class="nav-item active"><a class="nav-link" href="<?=base_url();?>index.php/toko/riwayatTukarPoin">Riwayat Tukar Poin</a></li>
                            <?php } else { ?>
                            <li class="nav-item"><a class="nav-link" href="<?=base_url();?>index.php/toko/riwayatTukarPoin">Riwayat Tukar Poin</a></li>
                            <?php } ?>
							<?php if($this->session->userdata('status_login') == 'login') { ?>
								<li class="nav-item"><a class="nav-link" href="<?=base_url();?>index.php/login/logout_nasabah">Logout</a></li>
							<?php } else { ?>
								<li class="nav-item"><a class="nav-link" href="<?=base_url();?>index.php/login/">Login</a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		</div>
	</header>
	<!-- End Header Area -->