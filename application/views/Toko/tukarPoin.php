	<!-- start banner Area -->
	<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				
						<!-- single-slide -->
						<div class="row single-slide">
							<div class="col-lg-5">
								<div class="banner-content">
									<br><br><h1>Tukar Poin<br></h1>
									<p>Kumpulkan poin sebanyak banyak nya untuk dapatkan barang barang eksklusif!</p>
									<div class="add-bag d-flex align-items-center">
										<!-- <a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase">Add to Bag</span> -->
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="teflon-banner">
								<img class="img-fluid" style="margin-left:230px;width:300px" src="<?= base_url();?>assets/img/star.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- start product Area -->
	<section class="">
		<!-- single product slide -->
		<div class="single-product">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<div class="section-title">
							<h1>Barang Yang Dapat Anda Tukarkan Dengan Poin Anda</h1>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- single product -->
                    <?php foreach ($poin as $poin) { ?>
					<div class="col-lg-3 col-md-6">
						<div class="single-product">
							<center><a href= "<?= base_url();?>index.php/toko/detailBarang/<?=$poin['id'];?>"><img class="img-fluid" style="height:130px;width:130px" src="<?= base_url();?>assets/barang/<?= $poin['foto'];?>" alt=""></a></center>
							<div class="product-details">
								<center><h6><?= $poin['nama_barang'];?>
									</h6></center>
								<div class="price">
									<center><h5><img src="<?=base_url();?>assets/img/star.png" style="width:25px"><?= $poin['poin'];?> Poin</h5></center>
								</div>
								<div class="price">
									<center><h6>Sisa Stok : <?= $poin['stok']; ?></h6></center>
								</div>
								<div class="prd-bottom">
									<center>
									<?php if($poin['stok'] == '0') { ?>
									<button DISABLED class="btn btn-fill btn-danger">MAAF STOK HABIS</button>
									<?php } else { ?>
									<a href="<?=base_url();?>index.php/toko/detailBarangPoin/<?=$poin['id'];?>" class="btn btn-fill btn-success"><i class="fa fa-shopping-basket"> Lihat</i></a></center>
									<?php } ?>

								</div>
							</div>
						</div>
					</div>
                    <?php } ?>
				</div>
			</div>
		</div>
		
				
				
					<!-- single product -->
					
								<div class="prd-bottom">

									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end product Area -->

	