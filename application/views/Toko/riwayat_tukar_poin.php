	<!-- start banner Area -->
	<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				
						<!-- single-slide -->
						<div class="row single-slide">
							<div class="col-lg-5">
								<div class="banner-content">
									<h1>Riwayat Belanja<br></h1>
									<p></p>
									<div class="add-bag d-flex align-items-center">
										<!-- <a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase">Add to Bag</span> -->
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="teflon-banner">
                                <img class="img-fluid" style="margin-left:230px;width:300px" src="<?= base_url();?>assets/img/history.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- start product Area -->
	<section class="">
		<!-- single product slide -->
		<div class="single-product">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<div class="section-title">
							<h1>Riwayat Belanja Anda</h1>
						</div>
					</div>
				</div>
				<div class="content table-responsive table-full-width">
					<!-- single product -->
					<table id="example" border="1" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th><center>No</center></th>
                                    <th><center>Tanggal</center></th>
                                    <th><center>Nama Barang</center></th>
                                </tr>
                                <?php 
							$no=0;
							$over=0;
                           foreach ($riwayat as $riwayat) {
							   
                                $no++;
                                ?>
                                </tr>   
                            </thead>
                            <tbody>
                                <tr>
                                    <td><center><?= $no;?></center></td>
                                    <td><center><?= $riwayat['tanggal'];?></center></td>
                                    <td><center><?= $riwayat['nama_barang'];?></center></td>
                                   
                                </tr>
                            </tbody>
                            <?php } ?>
                        </table><br>
							
				</div>
			</div>
		</div>
		
		<!-- single product slide -->
	
								
								<div class="prd-bottom">

									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end product Area -->
	<script>
	$(document).ready(function() {

	$('#example').DataTable( {
		dom: 'Bfrtip',
		buttons: [
	'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
	]
	});
});

	</script>

	