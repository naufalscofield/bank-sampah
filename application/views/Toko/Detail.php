<!-- Start Banner Area -->
<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				
						<!-- single-slide -->
						<div class="row single-slide">
							<div class="col-lg-5">
								<div class="banner-content">
									<h1>Detail Barang</h1>
									<div class="add-bag d-flex align-items-center">
										<!-- <a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase">Add to Bag</span> -->
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="teflon-banner">
									<!-- <img class="img-fluid" src="<?= base_url();?>assets/toko/img/banner/teflon-banner.png" alt=""> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!--================Single Product Area =================-->
<?php foreach ($barang as $barang) { ?>
	<div class="product_image_area">
		<div class="container">
			<div class="row s_product_inner">
				<div class="col-lg-6">
					<div class="single-product">
						<div class="single-prd-item">
							<img class="img-fluid" src="<?= base_url();?>assets/barang/<?= $barang['foto'];?>" alt="">
						</div>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<div class="s_product_text">
						<h3><?= $barang['nama_barang'];?></h3>
						<h2>>Rp.<?= number_format($barang['harga'],2,',','.'); ?></h2>
						<ul class="list">
							<li><a class="active" href="#"><span>Ketegori</span> : <?= $barang['nama_kategori'];?></a></li>
							<li><a href="#"><span>Stok</span> : <?= $barang['stok'];?></a></li>
						</ul>
						<p><?= $barang['deskripsi'];?></p>
						<div class="product_count">
                        <form action="<?=base_url();?>index.php/toko/addToCart" method="post">
							<label for="qty">Quantity:</label>
							<input required type="number" name="qty" id="sst" max="<?= $barang['stok'];?>" value="1" title="Quantity:" class="input-text qty">
                            <input type="hidden" name="id_barang" value="<?= $barang['id_barang'];?>">
                            <input type="hidden" name="harga" value="<?= $barang['harga'];?>">
                            <input type="hidden" name="nama_barang" value="<?= $barang['nama_barang'];?>">
                        </div>
						<div class="card_area d-flex align-items-center">
                        <?php if($barang['stok'] == '0'){?>
                            <button DISABLED class="btn btn-fill btn-danger">MAAF STOK HABIS</button>
                        <?PHP } else { ?>
                            <button type="submit" class="primary-btn">Tambah Ke Keranjang</button>
                        <?php } ?>
                            </form>
                            <!-- <a class="primary-btn" href="#">Tambah Ke Keranjang</a> <br> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<!--================End Single Product Area =================-->