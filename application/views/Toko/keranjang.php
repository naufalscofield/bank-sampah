	<!-- start banner Area -->
	<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				
						<!-- single-slide -->
						<div class="row single-slide">
							<div class="col-lg-5">
								<div class="banner-content">
									<h1>Keranjang Belanja<br></h1>
									<p>Yakin segini aja belanja nya?Gamau tambah?</p>
									<div class="add-bag d-flex align-items-center">
										<!-- <a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase">Add to Bag</span> -->
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="teflon-banner">
									<img class="img-fluid" style="margin-left:150px;width:400px" src="<?= base_url();?>assets/img/cart.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- start product Area -->
	<section class="">
		<!-- single product slide -->
		<div class="single-product">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<div class="section-title">
							<h1>Keranjang Anda</h1>
						</div>
					</div>
				</div>
				<div class="content table-responsive table-full-width">
					<!-- single product -->
					<table id="example" border="1" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th><center>No</center></th>
                                    <th><center>Nama Barang</center></th>
                                    <th><center>Jumlah</center></th>
                                    <th><center>Stok Tersedia</center></th>
                                    <th><center>Harga</center></th>
                                    <th><center>Total</center></th>
                                    <th><center>Aksi</center></th>
                                    <th><center>Keterangan</center></th>
                                </tr>
                                <?php 
							$no=0;
							$over=0;
                           foreach ($this->cart->contents() as $items) {
							   $total = $items['qty'] * $items['price'];
                                $no++;
                                ?>
                                </tr>   
                            </thead>
                            <tbody>
                                <tr>
                                    <td><center><?= $no;?></center></td>
                                    <td><center><?= $items['name'];?></center></td>
                                    <td><center><?= $items['qty'];?></center></td>
                                    <td><center><?= $items['stok'];?></center></td>
                                    <td><center>Rp.<?= number_format($items['price'],2,',','.'); ?></center></td>
                                    <td><center>Rp.<?= number_format($total,2,',','.'); ?></center></td>
                                    <td><center>
									<form action="<?=base_url();?>index.php/toko/kurangiItem" method="post">
									<input type="hidden" name="rowid" value="<?= $items['rowid'];?>">
									<input type="hidden" name="qty" value="<?= $items['qty'];?>">
									<input type="text" required name="kurang" class="form-control border-input" style="width:50px" value="">
									<input type="submit" class="btn brn-fill btn-warning" name="" value="Kurangi">
									</form>
									<a href='<?= base_url();?>index.php/toko/hapuscart/<?= $items['rowid'];?>' class='btn btn-danger'><i class='fa fa-times'></i> Batal</a>
									</td></center>
									<?php if($items['qty'] > $items['stok']){
										$over++;
										?>
									<td><center>JUMLAH PEMBELIAN MELEBIHI STOK!</center></td>
									<?php } else { ?>
									<td><center>-</center></td>
									<?php } ?>
                                </tr>
                            </tbody>
                            <?php } ?>
                        </table><br>
							<input type="hidden" value="<?=$over;?>">
							<center><h3>Subtotal : Rp.<?= number_format($this->cart->total(),2,',','.'); ?></h3></center><br>
							<center><h3>Saldo Anda</h3></center><br>
							<?php 
							$id_user = $this->session->userdata('id');
							$saldo = $this->db->get_where('tb_nasabah',array('id' => $id_user))->row();
							$saldoFix = $saldo->saldo;
							?>
							<center><h2>Rp.<?= number_format($saldoFix,2,',','.'); ?></h2></center><br>
							<?php if($this->cart->total() > $saldoFix){?>
							<center><button disabled class="btn btn-fill btn-danger">MAAF SALDO ANDA TIDAK MENCUKUPI UNTUK CHECKOUT</BUTTON></center><br>
							<?PHP } else { ?>
							<center><a href="<?= base_url();?>index.php/toko/checkout/<?=$over;?>/<?= $this->cart->total();?>" class="btn btn-fill btn-success">CHECKOUT</a></center><br>
							<?PHP } ?>
				</div>
			</div>
		</div>
		
		<!-- single product slide -->
	
								
								<div class="prd-bottom">

									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end product Area -->
	<script>
	$(document).ready(function() {

	$('#example').DataTable( {
		dom: 'Bfrtip',
		buttons: [
	'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
	]
	});
});

	</script>

	