	<!-- start banner Area -->
	<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-start">
				
						<!-- single-slide -->
						<div class="row single-slide">
							<div class="col-lg-5">
								<div class="banner-content">
									<h1>Belanja Pake Sampah?<br>Bisa!</h1>
									<p>Kumpulkan sampah sampah kamu dan tukarkan dengan saldo untuk berbelanja di Bank Sampah. Kumpulkan juga point sebanyak banyak nya untuk dapatkan barang barang eksklusif!</p>
									<div class="add-bag d-flex align-items-center">
										<!-- <a class="add-btn" href=""><span class="lnr lnr-cross"></span></a>
										<span class="add-text text-uppercase">Add to Bag</span> -->
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="teflon-banner">
									<img class="img-fluid" src="<?= base_url();?>assets/toko/img/banner/online-shop.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- start product Area -->
	<section class="">
		<!-- single product slide -->
		<div class="single-product">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6 text-center">
						<div class="section-title">
							<h1>Produk Produk Kami</h1>
							<p>Kualitas terbaik, harga pasti terjangkau!.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- single product -->
                    <?php foreach ($barang as $barang) { ?>
					<div class="col-lg-3 col-md-6">
						<div class="single-product">
							<center><a href= "<?= base_url();?>index.php/toko/detailBarang/<?=$barang['id'];?>"><img class="img-fluid" style="height:130px;width:130px" src="<?= base_url();?>assets/barang/<?= $barang['foto'];?>" alt=""></a></center>
							<div class="product-details">
								<center><h6><?= $barang['nama_barang'];?>
									</h6></center>
								<div class="price">
									<center><h5>Rp.<?= number_format($barang['harga'],2,',','.'); ?></h5></center>
								</div>
								<div class="price">
									<center><h6>Sisa Stok : <?= $barang['stok']; ?></h6></center>
								</div>
								<div class="prd-bottom">
									<center>
									<form action="<?=base_url();?>index.php/toko/addToCart" method="post">
									<input type="number" required placeholder="Qty" max="<?=$barang['stok'];?>" name="qty"><br>
									<input type="hidden" value="<?=$barang['id'];?>" name="id_barang"><br>
									<input type="hidden" value="<?=$barang['nama_barang'];?>" name="nama_barang"><br>
									<input type="hidden" value="<?=$barang['harga'];?>" name="harga"><br>
									<?php if($barang['stok'] == '0') { ?>
									<button DISABLED class="btn btn-fill btn-danger">MAAF STOK HABIS</button>
									<?php } else { ?>
									<button type="submit" class="btn btn-fill btn-success"><i class="fa fa-shopping-basket"> Tambah Ke Keranjang</i></button></center>
									<?php } ?>
									</form>
								</div>
							</div>
						</div>
					</div>
                    <?php } ?>
				</div>
			</div>
		</div>
		
				
				
					<!-- single product -->
					
								<div class="prd-bottom">

									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end product Area -->

	