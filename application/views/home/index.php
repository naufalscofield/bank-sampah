
            <div class="section section-gray section-clients">
                <div class="container text-center">
                    <h4 class="header-text">Partner Bisnis</h4>
                    <p>
                    Jadilah bagian dari Misi Kami untuk meningkatkan angka daur ulang sampah di Kecamatan Ngamprah. Ciptakan Kecamatan Ngamprah yang hijau kembali.<br>
                    </p>
                    <div class="logos">
                        <ul class="list-unstyled">
                            <li ><img src="<?=base_url();?>assets/home/img/logos/adobe.png"/></li>
                            <li ><img src="<?=base_url();?>assets/home/img/logos/zendesk.png"/></li>
                            <li ><img src="<?=base_url();?>assets/home/img/logos/ebay.png"/></i>
                            <li ><img src="<?=base_url();?>assets/home/img/logos/evernote.png"/></li>
                            <li ><img src="<?=base_url();?>assets/home/img/logos/airbnb.png"/></li>
                            <li ><img src="<?=base_url();?>assets/home/img/logos/zappos.png"/></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="section section-presentation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="description">
                                <h4 class="header-text">Jual Sampah</h4>
                                <p>Sudah memilah sampah tapi tidak tahu mau dikelola kemana dan dimana? Bank Sampah Kreatif punya solusinya! Booking permintaan pickup melalui website, petugas akan menjemput sampahmu. Ciptakan dunia yang lebih hijau. Daur Ulang Sekarang!</p>
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1 hidden-xs">
                            <img src="<?=base_url();?>assets/home/img/template/trash.png"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-demo">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="description-carousel" class="carousel fade" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item">
                                        <img src="<?=base_url();?>assets/home/img/template/examples/tong.jpg" alt="">
                                    </div>
                                    <div class="item active">
                                        <img src="<?=base_url();?>assets/home/img/template/examples/ember.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="<?=base_url();?>assets/home/img/template/examples/bg7.jpg" alt="">
                                    </div>
                                </div>
                                <ol class="carousel-indicators carousel-indicators-blue">
                                    <li data-target="#description-carousel" data-slide-to="0" class=""></li>
                                    <li data-target="#description-carousel" data-slide-to="1" class="active"></li>
                                    <li data-target="#description-carousel" data-slide-to="2" class=""></li>
                                </ol>
                            </div>
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <h4 class="header-text">Sumbang Sampah</h4>
                            <p>
                            Kesulitan mengelola sampah secara berkelanjutan di perusahaan anda? Coba program sumbang sampah, berlangganan dari Bank Sampah Kreatif. Kurangi dan daur ulang lebih banyak sampah. Buat dunia hijau kembali!
                            </p>
                            <!--a href="http://www.creative-tim.com/product/awesome-landing-page" id="Demo3" class="btn btn-fill btn-info" data-button="info">Get Free Demo</a-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-features">
                <div class="container">
                    <h4 class="header-text text-center">Langkah Mudah Jual Sampah Anda</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card card-blue">
                                <div class="icon">
                                <i><img src="<?=base_url();?>assets/home/img/logos/garbage.png"/></i>
                                </div>
                                <div class="text">
                                    <h4>Pilah Sampah Anda</h4>
                                    <p>Pisahkan sampah anorganik yang akan anda tukarkan sesuai dengan jenisnya (min. total berat = 1kg).</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-blue">
                                <div class="icon">
                                <i><img src="<?=base_url();?>assets/home/img/logos/notification.png"/></i>
                                </div>
                                <h4>Beritahu Kami Jika Sampah Sudah Siap Disetorkan</h4>
                                <p>Setelah sampah sudah siap disetorkan, beritahu kami melalui form yang telah disediakan. </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-blue">
                                <div class="icon">
                                <i><img src="<?=base_url();?>assets/home/img/logos/truck.png"/></i>
                                </div>
                                <h4>Tunggu Petugas Pengangkut Datang</h4>
                                <p>Petugas akan segera menjemput sampah anda. Kemudian akan memasukan data sampah anda yang akan menjadi saldo di akun anda.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section section-testimonial">
                <div class="container">
                    <h4 class="header-text text-center">Apa Yang Orang Lain Pikirkan</h4>
                    <div id="carousel-example-generic" class="carousel fade" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item">
                                <!--div class="mask">
                                    <img src="<?=base_url();?>assets/home/img/faces/face-4.jpg">
                                </div-->
                                <div class="carousel-testimonial-caption">
                                    <p>Siti Julaeha, Warga</p>
                                    <h3>"Bisa bayar iuran bulanan RW menggunakan sampah"</h3>
                                </div>
                            </div>
                            <div class="item active">
                                <!--div class="mask">
                                    <img src="<?=base_url();?>assets/home/img/faces/face-3.jpg">
                                </div-->
                                <div class="carousel-testimonial-caption">
                                    <p>Facebook, Donatur</p>
                                    <h3>"Senang bisa bekerjasama dengan Bank Sampah Kreatif"</h3>
                                </div>
                            </div>
                            <!--div class="item">
                                <div class="mask">
                                    <img src="<?=base_url();?>assets/home/img/faces/face-2.jpg">
                                </div>
                                <div class="carousel-testimonial-caption">
                                    <p>Rick Ross, Musician</p>
                                    <h3>"Loving this! Just picked it up the other day. Thank you for the work you put into this."</h3>
                                </div>
                            </div-->
                        </div>
                        <ol class="carousel-indicators carousel-indicators-blue">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>
                    </div>
                </div>
            </div>
            <!--div class="section section-no-padding">
                <div class="parallax filter-gradient blue" data-color="blue">
                    <div class="parallax-background">
                        <img class ="parallax-background-image" src="<?=base_url();?>assets/home/img/template/bg3.jpg"/>
                    </div>
                    <div class="info">
                        <h1>Download this landing page for free!</h1>
                        <p>Beautiful multipurpose bootstrap landing page.</p>
                        <a href="http://www.creative-tim.com/product/awesome-landing-page" class="btn btn-neutral btn-lg btn-fill">DOWNLOAD</a>
                    </div>
                </div>
            </div-->
