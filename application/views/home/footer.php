<footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <!--ul>
                            <li>
                                <a href="#">
                                Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Blog
                                </a>
                            </li>
                        </ul-->
                    </nav>
                    <div class="social-area pull-right">
                        <!--a class="btn btn-social btn-facebook btn-simple">
                        <i class="fa fa-facebook-square"></i>
                        </a>
                        <a class="btn btn-social btn-twitter btn-simple">
                        <i class="fa fa-twitter"></i>
                        </a>
                        <a class="btn btn-social btn-pinterest btn-simple">
                        <i class="fa fa-pinterest"></i>
                        </a-->
                    </div>
                    <div class="copyright">
                        &copy; 2019 <a href="https://jessicamiqo.blogspot.com/">Tugas Akhir</a>, made by jessica miqo
                    </div>
                </div>
            </footer>
        </div>

    </body>
    <script src="<?=base_url();?>assets/home/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="<?=base_url();?>assets/home/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>assets/home/js/bootstrap.js" type="text/javascript"></script>
    <script src="<?=base_url();?>assets/home/js/awesome-landing-page.js" type="text/javascript"></script>
</html>
