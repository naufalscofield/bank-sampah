<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="<?=base_url();?>assets/home/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Bank Sampah</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <script type="text/javascript" src="<?php echo base_url();?>assets/jquery/jquery.min.js"></script>
        <link href="<?=base_url();?>assets/home/css/bootstrap.css" rel="stylesheet" />
        <link href="<?=base_url();?>assets/home/css/landing-page.css" rel="stylesheet"/>
        <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet" />

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="<?=base_url();?>assets/home/css/pe-icon-7-stroke.css" rel="stylesheet" />

        

    </head>
    <div class="section section-gray section-clients">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <center><h4 class="header-text">Status dan Riwayat Setor Anda</h4></center><br>
                    <hr>
                   <div style="width:800px; margin:0 auto;">
                   <table id="example" class="display" style="width:900" cellpading="10" cellspacing="1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Petugas</th>
                                    <th>Jenis Request</th>
                                    <th>Tanggal Submit</th>
                                    <th>Tanggal Diambil</th>
                                    <th>Deskripsi Sampah</th>
                                    <th>Ukuran Sampah</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                                <?php 
                            $no=0;
                            foreach ($request as $request) {
                                $no++;
                                ?>
                                </tr>   
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $no;?></td>
                                    <?php if($request['status'] == 'pending') { ?>
                                    <td><i>Belum Ditentukan</i></td>
                                    <?php } else { ?>
                                    <td><?= $request['nama_petugas'];?></td>
                                    <?php } ?>
                                    <td><?= $request['jenis_request'];?></td>
                                    <td><?= $request['tanggal'];?></td>
                                    <td><?= $request['tanggal_diambil'];?></td>
                                    <td><?= $request['deskripsi'];?></td>
                                    <td><?= $request['ukuran_sampah'];?></td>
                                    <td><?= $request['status'];?></td>
                                    <?php if ($request['status'] == 'pending') { ?>
                                    <td><a href='<?= base_url();?>index.php/welcome/batal_request/<?= $request['id'];?>' class='btn btn-danger'><i class='fa fa-times'></i> Batal Request</a>
                                    <?php } else { ?>
                                    <td></td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>    
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
        <script>
            $(document).ready(function() {
                $('#example').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
            ]
                });
            });
        </script>
    </footer>