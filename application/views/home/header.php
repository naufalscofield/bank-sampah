<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="<?=base_url();?>assets/home/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Bank Sampah</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="<?=base_url();?>assets/home/css/bootstrap.css" rel="stylesheet" />
        <link href="<?=base_url();?>assets/home/css/landing-page.css" rel="stylesheet"/>

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="<?=base_url();?>assets/home/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <body class="landing-page landing-page1">
        <nav class="navbar navbar-transparent navbar-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                    </button>
                    <?php
                    if ($this->session->userdata('status_login') == 'login'){ ?>
                    <a href="<?=base_url();?>index.php/welcome/profil">
                        <div class="logo-container">
                            <div class="logo">
                                <img src="<?=base_url();?>assets/home/img/new_logo.png" alt="Bank Sampah user">
                            </div>
                            <div class="brand">
                                <?= $this->session->userdata('nama_lengkap');?>    
                            </div>
                        </div>
                    </a>
                    <?php } ?>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="example" >
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?= base_url();?>index.php/toko">
                            <i class="fa fa-tags"></i>
                            Toko
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url();?>index.php/welcome/request">
                            <i class="fa fa-bell"></i>
                            Setor Sampah
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url();?>index.php/welcome/status_request">
                            <i class="fa fa-th"></i>
                            Status dan Riwayat Setor
                            </a>
                        </li>
                        <?php
                        if ($this->session->userdata('status_login') == 'login'){ ?>
                        <li>
                            <a href="<?=base_url();?>index.php/login/logout_nasabah">
                            <i class="fa fa-sign-out"></i>
                            Logout
                            </a>
                        </li>
                        <?php  } else { ?>
                        <li>
                            <a href="<?=base_url();?>index.php/login">
                            <i class="fa fa-sign-in"></i>
                            Login
                            </a>
                        </li>
                        <?php } ?>    
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="wrapper">
            <div class="parallax filter-gradient blue" data-color="blue">
                <div class="parallax-background">
                    <img class="parallax-background-image" src="<?=base_url();?>assets/home/img/template/bg4.jpg">
                </div>
                <div class= "container">
                    <div class="row">
                        <div class="col-md-5 hidden-xs">
                            <div class="parallax-image">
                                <img class="phone" src="<?=base_url();?>assets/home/img/template/trashcan3.png" style="margin-top: 20px"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                            <div class="description">
                                <h2>Bank Sampah Kreatif</h2>
                                <br>
                                <h5>Kini Semua Orang Bisa Mendapat Uang dari Sampah. Ayo Tukarkan Sampah Anda Sekarang di Bank Sampah Kreatif! Dapatkan Banyak Keuntungan Sekarang Juga.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            