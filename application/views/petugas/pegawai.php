<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Pegawai</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#addPegawai"><i class="fa fa-plus"></i></button></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Lengkap</b></center></th>
                                            <th width="20"><center><b>Username</b></center></th>
                                            <th width="20"><center><b>No Telp</b></center></th>
                                            <th width="20"><center><b>Alamat</b></center></th>
                                            <th width="20"><center><b>Jabatan</b></center></th>
                                            <th width="20"><center><b>Status</b></center></th>
                                            <th width="20"><center><b>Status</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($pegawai as $pegawai) {
                                            $no++; ?>
                                            <?php if ($pegawai['jabatan'] == 'admin'){?>

                                        <tr style="background-color:lime">
                                            <?php } else{?>
                                        <tr>
                                            <?php } ?>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['nama_lengkap']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['username']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['no_telp']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['alamat']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['jabatan']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['status']; ?></b></center></td>
                                            <td width="20"><center><b><?= $pegawai['tersedia']; ?></b></center></td>
                                            <td width="20"><center>
                                            <a href='<?= base_url();?>index.php/admin/deletePegawai/<?= $pegawai['id'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                                            <a id='btn_edit' data-id_edit='<?= $pegawai['id']; ?>' data-nama_lengkap_edit='<?= $pegawai['nama_lengkap']; ?>' class='btn btn-info' data-toggle='modal' data-target='#editPegawai'><i class='fa fa-pencil'></i></a>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="addPegawai" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Tambah Pegawai</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>index.php/admin/insertPegawai" method="post" enctype="multipart/form-data">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Pegawai</label>
                                        <input type="text" required id="nama_lengkap" class="form-control border-input" placeholder="Nama Lengkap" value="" name="nama_lengkap">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" required id="username" class="form-control border-input" placeholder="Username" value="" name="username">
                                    </div>
                                </div>
                            </div>

        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No Telp</label>
                                        <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required id="no_telp" class="form-control border-input" placeholder="No Telp" value="" name="no_telp">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <select name="jabatan" required class="form-control border-input" id="jabatan">
                                            <option value="">-- Pilih Jabatan --</option>
                                            <option value="admin">Admin</option>
                                            <option value="petugas">Petugas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Alamat</label>
                                        <textarea name="alamat" id="alamat" class="form-control border-input" required cols="30" rows="10"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file" required id="foto" class="form-control border-input" placeholder="No Telp" value="" name="foto">
                                    </div>
                                </div>
                            </div>    
                        
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                    Tambah Pegawai
                                </button>
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="editPegawai" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Edit Pegawai</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>index.php/admin/updatePegawai">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">

                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_nama_lengkap">
                                        
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="form-group" id="input_username">
                                        
                                    </div>
                                </div>
                            </div>  
 
                               
   
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_no_telp">
                                        
                                    </div>
                                </div>

                            <div class="col-md-6">
                                    <div class="form-group" id="input_jabatan">
                                        <label>Jabatan</label>
                                        <select name="jabatan_edit" required class="form-control border-input" id="jabatan_edit">
                                            <option value="">-- Pilih Jabatan --</option>
                                            <option value="admin">Admin</option>
                                            <option value="petugas">Petugas</option>
                                        </select>
                                    </div>
                                </div>
                            </div>    
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="input_alamat">
                                        
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group" id="input_jabatan">
                                        <label>Status</label>
                                        <select name="status_edit" required class="form-control border-input" id="status_edit">
                                            <option value="">-- Pilih Status --</option>
                                            <option value="aktif">Aktif</option>
                                            <option value="tidak_aktif">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>    

                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Edit Pegawai
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>
    </div>
</div>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
  ]
    } );

    $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("id_edit");
                console.log(id)
                $.get("http://localhost/banksampah/index.php/admin/showPegawai/" + id, function(data, status){
                console.log(status)
                data = JSON.parse(data);
                console.log(data)
                if (status) {
                    data.forEach(element => {
                    $('#input_id').html("")
                        $('#input_id').append("<input type='hidden' required id='id_edit' class='form-control border-input' name='id_edit' value="+element.id+">");
                    $('#input_nama_lengkap').html("")
                        $('#input_nama_lengkap').append("<label>Nama Lengkap Pegawai</label><input type='text' required id='nama_lengkap_edit' class='form-control border-input' name='nama_lengkap_edit' value='"+element.nama_lengkap+"'>");
                    $('#input_username').html("")
                        $('#input_username').append("<label>Username</label><input type='text' required id='username_edit' class='form-control border-input' name='username_edit' value="+element.username+">");
                    $('#input_no_telp').html("")
                        $('#input_no_telp').append("<label>No Telp</label><input type='text' required id='no_telp_edit' class='form-control border-input' name='no_telp_edit' value="+element.no_telp+">");
                    $('#input_alamat').html("")
                        $('#input_alamat').append("<label for=''>Alamat</label><textarea name='alamat_edit' id='alamat_edit' class='form-control border-input' required cols='30' rows='10'>"+element.alamat+"</textarea>");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });
            });  

} );
</script>