<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Request Panggilan Sumbang Sampah</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Nasabah</b></center></th>
                                            <!-- <th width="20"><center><b>Alamat Nasabah</b></center></th> -->
                                            <th width="20"><center><b>Petugas</b></center></th>
                                            <th width="20"><center><b>Jenis Request</b></center></th>
                                            <th width="20"><center><b>Status</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($request as $request) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $request['nama_nasabah']; ?></b></center></td>
                                            <!-- <td width="20"><center><b><?= $request['alamat_nasabah']; ?></b></center></td> -->
                                            <td width="20"><center><b><?= $request['nama_petugas']; ?></b></center></td>
                                            <td width="20"><center><b><?= $request['jenis_request']; ?></b></center></td>
                                            <td width="20"><center><b><?= $request['status']; ?></b></center></td>
                                            <td width="20"><center>
                                            <a id='btn_edit' data-id_edit='<?= $request['id_nasabah']; ?>' data-nama_lengkap_edit='<?= $nasabah['nama_lengkap']; ?>' class='btn btn-info' data-toggle='modal' data-target='#editNasabah'><i class='fa fa-pencil'></i></a>
                                            <?php
                                                if($request['status'] == 'diproses'){?>
                                            <form action="<?= base_url();?>index.php/petugas/updateStatusRequest" method="post">
                                            <input type="hidden" name="id" value="<?= $request['id'];?>">
                                            <button class='btn btn-info' type="submit"><i class='fa fa-arrow-circle-up'></i>
                                            </form>    
                                            <?php }else{?>
                                            <a id='btn_cart' class='btn btn-warning' data-toggle='modal' data-target='#cartSampah'><i class='fa fa-shopping-basket'></i></a>
                                            <a href='<?=base_url();?>index.php/petugas/toCheckoutSumbang/<?= $request['id']; ?>' class='btn btn-success'><i class='fa fa-check'></i></a>
                                            <?php } ?>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="editNasabah" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Alamat</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>index.php/admin/updateNasabah">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_alamat">
                                        
                                    </div>
                                </div>
                            </div>    

                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

            <div id="cartSampah" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Input Sampah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="" method="post" action="<?= base_url();?>index.php/petugas/cartSumbang">
                        <!-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">

                                    </div>
                                </div>
                            </div>   -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <label for="">Jenis Sampah</label>
                                    <input type="text" required name="jenis_sampah" class="form-control border-input">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <label for="">Satuan Jenis Sampah</label>
                                    <input type="text" required name="satuan" class="form-control border-input">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <label for="">Berat Total</label>
                                    <input type="text" required name="berat" class="form-control border-input">
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="">
                                    <label for="">Keterangan</label>
                                   <textarea name="keterangan" class="form-control border-input" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>

                        

                        <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Masuk Keranjang Sampah
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <hr>
    </div>
</div>

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {

    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
    ]
    });

    $('#id_jenis_sampah').on('change', function() {

        $.ajax({
            type: "POST",
            data: { id_jenis_sampah: $('#id_jenis_sampah').val() },
            url: '<?php echo base_url()."index.php/petugas/getHargaSampah" ?>',
            dataType: 'text',
            success: function(resp) {
            var json = JSON.parse(resp.replace(',.', ''))
            var $h = $("#harga");
            var $s = $("#satuan");
            $h.empty(); // remove old options
            $s.empty(); // remove old options
            // $h.append($("<option></option>")
            // .attr("value", '').text('-- Pilih Matpel --'));
            $.each(json, function(key, value) {
                $h.append($("<label for='' id=''>Harga</label>"))
                $h.append($("<input type='text' id='harga' readonly name='harga' class='form-control border-input'>")
                .attr("value", value.harga));
                });
            $.each(json, function(key, value) {
                $s.append($("<label for='' id=''>Satuan</label>"))
                $s.append($("<input type='text' id='satuan' readonly name='satuan' class='form-control border-input'>")
                .attr("value", value.satuan));
                });
            $.each(json, function(key, value) {
                $s.append($("<input type='hidden' id='jenis_sampah' name='jenis_sampah' class='form-control border-input'>")
                .attr("value", value.jenis_sampah));
                });
            },
            error: function (jqXHR, exception) {
            console.log(jqXHR, exception)
            }
        });
    }); 

    $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("id_edit");
                console.log(id)
                $.get("http://localhost/banksampah/index.php/petugas/showNasabah/" + id, function(data, status){
                console.log(status)
                data = JSON.parse(data);
                console.log(data)
                if (status) {
                    data.forEach(element => {
                    $('#input_alamat').html("")
                        $('#input_alamat').append("<label for=''>Alamat</label><textarea readonly name='alamat_edit' id='alamat_edit' class='form-control border-input' required cols='30' rows='10'>"+element.alamat+"</textarea>");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });
            });    


       

       $(function(){
        $('#berat').on('change', function(){;
            var harga = $('#harga').val();
            console.log(harga);
            var berat = $('#berat').val();
            var total = harga * berat;

            $('#total').val(total);
        });
    });

});    
</script>