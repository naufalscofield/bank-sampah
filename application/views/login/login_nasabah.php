<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="<?=base_url();?>assets/home/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Bank Sampah</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <link href="<?=base_url();?>assets/home/css/bootstrap.css" rel="stylesheet" />
        <link href="<?=base_url();?>assets/home/css/landing-page.css" rel="stylesheet"/>

        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300' rel='stylesheet' type='text/css'>
        <link href="<?=base_url();?>assets/home/css/pe-icon-7-stroke.css" rel="stylesheet" />

    </head>
    <body class="landing-page landing-page1">
        <nav class="navbar navbar-transparent navbar-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar bar1"></span>
                    <span class="icon-bar bar2"></span>
                    <span class="icon-bar bar3"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                
                <!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="wrapper">
            <div class="parallax filter-gradient blue" data-color="blue">
                <div class="parallax-background">
                    <img class="parallax-background-image" src="<?=base_url();?>assets/home/img/template/bg3.jpg">
                </div>
                <div class= "container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="description">
                                <center><h2>Login Nasabah</h2></center>
                                <br>
                                <form action="<?=base_url();?>index.php/login/proses_nasabah" method="post">
                                    <label for="">Username</label><br>
                                    <input type="text" required class="form-control border-input" name="username"><br>
                                    <label for="">Password</label><br>
                                    <input type="password" required class="form-control border-input" name="password"><br>
                                    <center><button type="submit" class="btn btn-fill btn-success">Login</button></center>
                                </form>
                                <br>
                                <center><h5>Belum punya akun? <a href="<?=base_url();?>index.php/welcome/daftar">daftar disini</h5></center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            