<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_admin');
		if($this->session->userdata('status_login') != "login"){
			redirect(base_url("index.php/login/index_admin"));
		  }
	}

	public function getProfil()
	{
		$data['sektor'] = $this->model_admin->getSektor();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/profil',$data);
		$this->load->view('components_admin/footer');
	}

	public function updateProfil()
	{
		$this->model_admin->updateProfil();
		echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
	}

	public function changeFotoProfil()
	{
		$config['upload_path'] = './assets/user/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->model_admin->changeFotoProfil($name);
			echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
		}
	}

//---------------------------------------------------------------------------------------------------------------------//	

	public function getPegawai()
	{
		$data['sektor'] = $this->model_admin->getSektor();
		$data['sektor2'] = $this->model_admin->getSektor();
		$data['pegawai'] = $this->model_admin->getPegawai();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/pegawai',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertPegawai()
	{
		$config['upload_path'] = './assets/user/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];

		$this->model_admin->insertPegawai($name);
		redirect('index.php/admin/getPegawai');
		}
	}
	
	public function showPegawai($id)
	{
		$q = $this->db->get_where('tb_user',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updatePegawai()
	{
		$id = $this->input->post('id_edit');

		$data = array(
			'nama_lengkap' => $this->input->post('nama_lengkap_edit'),
			'username' => $this->input->post('username_edit'),
			'no_telp' => $this->input->post('no_telp_edit'),
			'alamat' => $this->input->post('alamat_edit'),
			'jabatan' => $this->input->post('jabatan_edit'),
			'status' => $this->input->post('status_edit'),
			'id_sektor' => $this->input->post('sektor_edit')
		);

		$this->model_admin->updatePegawai($id,$data);

		redirect('index.php/admin/getPegawai');
	}
	
	public function deletePegawai($id)
	{
		$this->model_admin->deletePegawai($id);
		redirect('index.php/admin/getPegawai');
	}

	//----------------------------------------------------------------------------------------------------------------//

	public function getBarang()
	{
		$data['barang'] = $this->model_admin->getBarang();
		$data['kategori'] = $this->model_admin->getKategori();
		$data['kategori_edit'] = $this->model_admin->getKategori();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/barang',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertBarang()
	{
		$config['upload_path'] = './assets/barang/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->model_admin->insertBarang($name);
			redirect('index.php/admin/getBarang');
		}
	}

	public function showBarang($id)
	{
		$q = $this->db->get_where('tb_barang',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateBarang()
	{
		$id = $this->input->post('id_edit');

		$config['upload_path'] = './assets/barang/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('input_foto')) {
			$this->model_admin->updateBarang($id);

			redirect('index.php/admin/getBarang');
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->model_admin->updateBarangG($id,$name);
			redirect('index.php/admin/getBarang');
		}
		
	}

	public function deleteBarang($id)
	{
		$this->model_admin->deleteBarang($id);
		redirect('index.php/admin/getBarang');
	}

	//------------------------------------------------------------------------------------------------------------------//

	public function getKategori()
	{
		$data['kategori'] = $this->model_admin->getKategori();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/kategori',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertKategori()
	{
		$this->model_admin->insertKategori();

		redirect('index.php/admin/getKategori');
	}

	public function showKategori($id)
	{
		$q = $this->db->get_where('tb_kategori',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateKategori()
	{
		$id = $this->input->post('id_edit');

		$this->model_admin->updateKategori($id);

		redirect('index.php/admin/getKategori');
	}

	public function deleteKategori($id)
	{
		$this->model_admin->deleteKategori($id);

		redirect('index.php/admin/getKategori');
	}

	//--------------------------------------------------------------------------------------------------------------//


	public function getJenisSampah()
	{
		$data['jenis'] = $this->model_admin->getJenisSampah();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/jenis_sampah',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertJenisSampah()
	{
		$this->model_admin->insertJenisSampah();

		redirect('index.php/admin/getJenisSampah');
	}

	public function showJenisSampah($id)
	{
		$q = $this->db->get_where('tb_jenis_sampah',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateJenisSampah()
	{
		$id = $this->input->post('id_edit');

		$this->model_admin->updateJenisSampah($id);

		redirect('index.php/admin/getJenisSampah');
	}

	public function deleteJenisSampah($id)
	{
		$this->model_admin->deleteJenisSampah($id);

		redirect('index.php/admin/getJenisSampah');
	}

	//--------------------------------------------------------------------------------------------------------------//

	public function getRequest()
	{
		$data['request'] = $this->model_admin->getRequest();
		$data['pegawai'] = $this->model_admin->getPegawaiKosong();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/request',$data);
		$this->load->view('components_admin/footer');
	}

	public function getPetugasSektor()
	{
		$id_nasabah = $this->input->post('id_nasabah');
		// print_r($id_nasabah); die;
		$sektor = $this->model_admin->getNasabahSektor($id_nasabah);
		$sektorFix = $sektor->id_sektor;

		if ($sektorFix == 0) {
			$wakwaw = $this->db->query("
			select * from tb_user where jabatan = 'petugas'")->result_array();
			$new = json_encode($wakwaw);
			echo $new;
		} else {

		$wakwaw = $this->db->query("
		select * from tb_user where jabatan = 'petugas' and id_sektor = $sektorFix")->result_array();
		$new = json_encode($wakwaw);
		echo $new;
		}
	}

	public function showRequest($id)
	{
		$q = $this->db->query('SELECT tb_request.id as id, tb_nasabah.id as id_nasabah, tb_nasabah.nama_lengkap as nama_nasabah, tb_nasabah.alamat as alamat_nasabah, tb_request.status as status , tb_user.id as id_user, tb_user.nama_lengkap as nama_petugas FROM `tb_request` left join tb_user on tb_user.id = tb_request.id_pegawai left join tb_nasabah on tb_nasabah.id = tb_request.id_nasabah where tb_request.id = '.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateRequest()
	{
		$id = $this->input->post('id_edit');

		$this->model_admin->updateRequest($id);

		redirect('index.php/admin/getRequest');
	}

	public function deleteRequest($id)
	{
		$this->model_admin->deleteRequest($id);

		redirect('index.php/admin/getRequest');
	}

	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiSampah()
	{
		if (!isset($_POST['date1'])){

			$data['transaksi'] = $this->model_admin->getTransaksiSampah();
	
			$this->load->view('components_admin/header');
			$this->load->view('components_admin/sidebar');
			$this->load->view('admin/transaksi_s',$data);
			$this->load->view('components_admin/footer');
		}else{
			$date1 = $this->input->post('date1');
			$date2 = $this->input->post('date2');
			$data['transaksi'] = $this->model_admin->getTransaksiSampahDate($date1,$date2);
	
			$this->load->view('components_admin/header');
			$this->load->view('components_admin/sidebar');
			$this->load->view('admin/transaksi_s',$data);
			$this->load->view('components_admin/footer');
		}
	}
	
	public function getSumbangSampah()
	{
		$data['transaksi'] = $this->model_admin->getSumbangSampah();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/sumbang_sampah',$data);
		$this->load->view('components_admin/footer');
	}

	public function showHibahSampah()
	{
		$id_sumbang_sampah_detail = $this->input->post('id_sumbang_sampah_detail');
		$this->model_admin->getDetailSumbangSampah($id_sumbang_sampah_detail);

		// $q = $this->db->query('SELECT * FROM tb_sumbang_sampah_detail where id_sumbang_sampah_detail ='.$id)->result_array();
		// // $q = $this->db->get_where('tb_sumbang_sampah_detail',array('id_sumbang_sampah_detail' => $id))->result_array();
		// $q = json_encode($q);
		// print_r($q); die;
        // return $q;
	}

	public function aksiHibahSampah()
	{

		$id_sumbang_sampah = $this->input->post('id_sumbang_sampah');
		$id_sumbang_sampah_detail = $this->input->post('id_sumbang_sampah_detail');
		$beratAwal = $this->db->get_where('tb_sumbang_sampah_detail',array('id_sumbang_sampah_detail' => $id_sumbang_sampah))->row();
		$beratAwalFix = $beratAwal->berat;
		$beratHibah = $this->input->post('berat');
		$beratAkhir = $beratAwalFix - $beratHibah;
		$id_nasabah = $beratAwal->id_nasabah;
		
		$data = array(
			'berat' => $beratAkhir
		);

		// $where = array(
		// 	'id_sumbang_sampah_detail' => $id_sumbang_sampah
		// );

		$this->db->where('id_sumbang_sampah_detail',$id_sumbang_sampah_detail);
		$this->db->update('tb_sumbang_sampah_detail',$data);
		//$this->model_admin->updateHibah($where,$data,'tb_sumbang_sampah_detail');
		
		$data2 = array(
			'id_perusahaan' => $id_nasabah,
			'id_umkm' => $this->input->post('id_umkm'),
			'jenis_sampah' => $this->input->post('jenis_sampah'),
			'berat' => $this->input->post('berat'),
			'satuan' => $this->input->post('satuan')
		);

		$this->db->insert('tb_hibah',$data2);

		redirect('index.php/admin/getSumbangSampah');
	}
	
	public function getHibah()
	{
		$this->db->join('tb_nasabah','tb_nasabah.id = tb_hibah.id_perusahaan');
		$this->db->join('tb_umkm','tb_umkm.id = tb_hibah.id_umkm');
		$data['hibah'] = $this->db->get('tb_hibah')->result_array();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/hibah',$data);
		$this->load->view('components_admin/footer');		
	}
	
	public function updateStatusTB($status,$id)
	{
		$this->db->where('id_transaksi_barang',$id);
		$data = array(
			'status_pengantaran' => $status
		);
		$this->db->update('tb_transaksi_barang',$data);
		redirect('index.php/admin/getTransaksiBarang');
	}

	
	// public function showHibahSampah($id)
	// {
	// 	$q = $this->db->query('SELECT tb_sumbang_sampah_detail.berat, tb_sumbang_sampah_detail.satuan, tb_sumbang_sampah_detail.jenis_sampah FROM tb_sumbang_sampah_detail inner join tb_jenis_sampah on tb_jenis_sampah.id = tb_sumbang_sampah_detail.id_jenis_sampah where id_transaksi_sampah ='.$id)->result_array();
	// 	$q = json_encode($q);
	// 	print_r($q); die;
    //     return $q;
	// }

	public function deleteTransaksiSampah($id)
	{
		$this->model_admin->deleteTransaksiSampah($id);
		$this->model_admin->deleteTransaksiSampah2($id);

		redirect('index.php/admin/getTransaksiSampah');
	}
	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiBarang()
	{
		$data['transaksi'] = $this->model_admin->getTransaksiBarang();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/transaksi_b',$data);
		$this->load->view('components_admin/footer');
	}

	public function showTransaksiBarang($id)
	{
		$q = $this->db->query('SELECT tb_transaksi_barang_detail.jumlah, tb_transaksi_barang_detail.total_harga, tb_barang.nama_barang FROM tb_transaksi_barang_detail inner join tb_barang on tb_barang.id = tb_transaksi_barang_detail.id_barang where id_transaksi_barang ='.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function deleteTransaksiBarang($id)
	{
		$this->model_admin->deleteTransaksiBarang($id);
		$this->model_admin->deleteTransaksiBarang2($id);

		redirect('index.php/admin/getTransaksiBarang');
	}

	//---------------------------------------------------------------------------------------------------------------------//

	public function getUmkm()
	{
		$data['umkm'] = $this->model_admin->getUmkm();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/umkm',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertUmkm()
	{
		$this->model_admin->insertUmkm();

		redirect('index.php/admin/getUmkm');
	}

	public function showUmkm($id)
	{
		$q = $this->db->get_where('tb_umkm',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateUmkm()
	{
		$id = $this->input->post('id_edit');

		$this->model_admin->updateUmkm($id);

		redirect('index.php/admin/getUmkm');
	}

	public function deleteUmkm($id)
	{
		$this->model_admin->deleteUmkm($id);

		redirect('index.php/admin/getUmkm');
	}

	//---------------------------------------------------------------------------------------------------------------------//	

	public function getNasabah()
	{
		$data['nasabah'] = $this->model_admin->getNasabah();
		$data['sektor'] = $this->model_admin->getSektor();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/nasabah',$data);
		$this->load->view('components_admin/footer');
	}

	public function insertNasabah()
	{
		$this->model_admin->insertNasabah();
		redirect('index.php/admin/getNasabah');
	}
	
	public function showNasabah($id)
	{
		$q = $this->db->get_where('tb_nasabah',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateNasabah()
	{
		$id = $this->input->post('id_edit');
		// print_r($id); die;

		$data = array(
			'status' => $this->input->post('status_edit')
		);

		$this->model_admin->updateNasabah($id,$data);

		redirect('index.php/admin/getNasabah');
	}
	
	public function deleteNasabah($id)
	{
		$this->model_admin->deleteNasabah($id);
		redirect('index.php/admin/getNasabah');
	}

	public function tukarUang()
	{
		$id = $this->input->post('id_edit');
		$password = $this->input->post('password_edit');
		$c_password = md5($this->input->post('c_password'));
		// print_r($password); die;

		if($c_password == $password){
			$this->model_admin->tukarUang($id);
			redirect('index.php/admin/getNasabah');
		} else {
			echo"<script>alert('Password tidak sesuai!'); window.location = '../admin/getNasabah'</script>";
		}
		// print_r($id); die;
	}

	public function getPendapatan()
	{
		$data['pp'] = $this->model_admin->getPendapatan();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/pendapatan',$data);
		$this->load->view('components_admin/footer');

	}
	
	public function getPenjualan()
	{
		$data['pp'] = $this->model_admin->getPenjualan();

		$this->load->view('components_admin/header');
		$this->load->view('components_admin/sidebar');
		$this->load->view('admin/penjualan',$data);
		$this->load->view('components_admin/footer');

	}

	public function getHargaSampah()
	{
		$id_jenis_sampah = $this->input->post('id_jenis_sampah');
		$this->model_admin->getHargaSampah($id_jenis_sampah);
	}
	
	public function getStokTersedia()
	{
		$id_jenis_sampah = $this->input->post('id_jenis_sampah');
		$masuk = $this->db->query("select sum(berat) as berat from tb_transaksi_sampah_detail where id_jenis_sampah = $id_jenis_sampah")->row();
		$keluar = $this->db->query("select sum(berat) as berat from tb_penjualan where id_jenis_sampah = $id_jenis_sampah")->row();
		
		if ($masuk->berat ==  null){
			$masukFix = 0;
		} else {
			$masukFix = $masuk->berat;
		}
		
		if ($keluar->berat ==  null){
			$keluarFix = 0;
		} else {
			$keluarFix = $keluar->berat;
		}

		$stok = $masukFix - $keluarFix;

		$stokTersedia = array(
			'stok' => $stok
		);

		$stokFix = json_encode($stokTersedia);
        echo $stokFix;
	}

	public function insertPenjualan()
	{
		$this->model_admin->insertPenjualan();
		$this->model_admin->insertPendapatan();

		redirect('index.php/admin/getPenjualan');
	}

}
