<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_petugas');
		$this->load->library('cart');
		if($this->session->userdata('status_login') != "login"){
			redirect(base_url("index.php/login/index_admin"));
		  }
	}

	public function getProfil()
	{
		$data['sektor'] = $this->model_petugas->getSektor();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/profil',$data);
		$this->load->view('components_petugas/footer');
	}

	public function updateProfil()
	{
		$this->model_petugas->updateProfil();
		echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
	}

	public function changeFotoProfil()
	{
		$config['upload_path'] = './assets/user/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('foto')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];
			$this->model_petugas->changeFotoProfil($name);
			echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
		}
	}

//---------------------------------------------------------------------------------------------------------------------//	

	public function getRequest()
	{
		$data['request'] = $this->model_petugas->getRequest();
		$data['jenis_sampah'] = $this->model_petugas->getJenisSampah();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/request',$data);
		$this->load->view('components_petugas/footer');
	}
	
	public function getRequestSumbang()
	{
		$data['request'] = $this->model_petugas->getRequestSumbang();
		$data['jenis_sampah'] = $this->model_petugas->getJenisSampah();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/request_sumbang',$data);
		$this->load->view('components_petugas/footer');
	}

	public function getHargaSampah()
	{
		$id_jenis_sampah = $this->input->post('id_jenis_sampah');
		$this->model_petugas->getHargaSampah($id_jenis_sampah);
	}

	public function updateStatusRequest()
	{
		$id = $this->input->post('id');
		$this->model_petugas->updateStatusRequest($id);

		redirect('index.php/petugas/getRequest');
	}

	public function cartSampah()
	{
		$berat = $this->input->post('berat');
		$harga = $this->input->post('harga');
		$total = $berat * $harga;
		// print_r($total); die;

		$data = array(
			'id'      				=> $this->input->post('id_jenis_sampah'),
			'name'      			=> $this->input->post('jenis_sampah'),
			'qty'     				=> $this->input->post('berat'),
			'satuan'     			=> $this->input->post('satuan'),
			'price'     			=> $this->input->post('harga'),
			'total'     			=> $total
		);
		
		$this->cart->insert($data);
		redirect('index.php/petugas/getRequest');
	}
	
	public function cartSumbang()
	{
		$berat = $this->input->post('berat');
		$harga = $this->input->post('harga');
		$total = $berat * $harga;
		// print_r($total); die;

		$data = array(
			'id'      				=> rand(001,999),
			'name'      			=> $this->input->post('jenis_sampah'),
			'satuan'     			=> $this->input->post('satuan'),
			'qty'     				=> $this->input->post('berat'),
			'price'     			=> 0,
			'keterangan'     		=> $this->input->post('keterangan')
		);

		// print_r($data); die;

			$this->cart->insert($data);
			redirect('index.php/petugas/getRequestSumbang');
	}
		
	public function deleteCart($rowid)
	{
			$data = array(
				'rowid' => $rowid,
				'qty' => 0
			);
			
			$this->cart->update($data);
			redirect('index.php/petugas/getRequest');
	}

	public function toCheckout($id)
	{
		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/checkout');
		$this->load->view('components_petugas/footer');
	}
	
	public function toCheckoutSumbang($id)
	{
		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/checkout_sumbang');
		$this->load->view('components_petugas/footer');
	}

	public function insertCheckout()
	{
		$id_request = $this->input->post('id_request');

		$totalBerat = $this->input->post('total_berat');
		
		$id_nasabah = $this->model_petugas->getIdNasabah($id_request);
		$id_nasabahFix = $id_nasabah->id_nasabah;

		$jenisNasabah = $this->db->get_where('tb_nasabah',array('id' => $id_nasabahFix))->row();
		$jenisNasabahFix = $jenisNasabah->jenis_nasabah;
		$poinAwalNasabah = $jenisNasabah->point;

		if($totalBerat >= 10){
			if($jenisNasabahFix == 'individual'){
				$poinDapat = 2;
			}else if($jenisNasabahFix == 'organisasi'){
				$poinDapat = 1;
			}
			else{
				$poinDapat = 0;
			}
		}else{
			$poinDapat = 0;	
		}

		$poinAkhir = $poinAwalNasabah + $poinDapat;

		
		$this->model_petugas->insertToTransaksi($id_nasabahFix);
		$this->model_petugas->insertPengeluaran();
		$this->model_petugas->updateSaldo($id_nasabahFix);
		$this->model_petugas->updatePoin($id_nasabahFix,$poinAkhir);
		$this->model_petugas->updateStatusRequestSelesai($id_request);

		
		foreach ($this->cart->contents() as $key) {
			
			$q = $this->db->query("SELECT max(id_transaksi_sampah) as id_transaksi from tb_transaksi_sampah")->row();
			$id_transaksi = $q->id_transaksi;

			$data=array(
                'id_transaksi_sampah' => $id_transaksi,
                'id_jenis_sampah' => $key['id'],
                'berat' => $key['qty'],
                'harga' => $key['price'],
            );
			$this->model_petugas->insertToTransaksiDetail($data);
		}
		$this->cart->destroy();
		redirect('index.php/petugas/getRequest');		
	}

	public function insertCheckoutSumbang()
	{
		$id_request = $this->input->post('id_request');

		
		
		$id_nasabah = $this->model_petugas->getIdNasabah($id_request);
		$id_nasabahFix = $id_nasabah->id_nasabah;

		$jenisNasabah = $this->db->get_where('tb_nasabah',array('id' => $id_nasabahFix))->row();
		$jenisNasabahFix = $jenisNasabah->jenis_nasabah;
		

		
		$this->model_petugas->insertToTransaksiSumbang($id_nasabahFix);
		$this->model_petugas->updateStatusRequestSelesai($id_request);

		
		foreach ($this->cart->contents() as $key) {
			
			$q = $this->db->query("SELECT max(id_sumbang_sampah) as id_sumbang from tb_sumbang_sampah")->row();
			$id_sumbang = $q->id_sumbang;

			$data=array(
                'id_sumbang_sampah' => $id_sumbang,
				'jenis_sampah' => $key['name'],
				'id_nasabah' => $id_nasabahFix,
				'tanggal' => date('Y-m-d'),
                'berat' => $key['qty'],
                'satuan' => $key['satuan'],
                'keterangan' => $key['keterangan'],
            );
			$this->model_petugas->insertToTransaksiDetailSumbang($data);
		}
		$this->cart->destroy();
		redirect('index.php/petugas/getRequestSumbang');		
	}

	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiSampah()
	{
		$data['transaksi'] = $this->model_petugas->getTransaksiSampah();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/transaksi_s',$data);
		$this->load->view('components_petugas/footer');
	}

	public function showTransaksiSampah($id)
	{
		$q = $this->db->query('SELECT tb_transaksi_sampah_detail.berat, tb_transaksi_sampah_detail.harga, tb_jenis_sampah.jenis_sampah FROM tb_transaksi_sampah_detail inner join tb_jenis_sampah on tb_jenis_sampah.id = tb_transaksi_sampah_detail.id_jenis_sampah where id_transaksi_sampah ='.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function deleteTransaksiSampah($id)
	{
		$this->model_petugas->deleteTransaksiSampah($id);
		$this->model_petugas->deleteTransaksiSampah2($id);

		redirect('index.php/petugas/getTransaksiSampah');
	}
	//--------------------------------------------------------------------------------------------------------------//

	public function getTransaksiBarang()
	{
		$data['transaksi'] = $this->model_petugas->getTransaksiBarang();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/transaksi_b',$data);
		$this->load->view('components_petugas/footer');
	}

	public function showTransaksiBarang($id)
	{
		$q = $this->db->query('SELECT tb_transaksi_barang_detail.jumlah, tb_transaksi_barang_detail.total_harga, tb_barang.nama_barang FROM tb_transaksi_barang_detail inner join tb_barang on tb_barang.id = tb_transaksi_barang_detail.id_barang where id_transaksi_barang ='.$id)->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function deleteTransaksiBarang($id)
	{
		$this->model_petugas->deleteTransaksiBarang($id);
		$this->model_petugas->deleteTransaksiBarang2($id);

		redirect('index.php/petugas/getTransaksiBarang');
	}

	//---------------------------------------------------------------------------------------------------------------------//	

	public function getNasabah()
	{
		$data['nasabah'] = $this->model_petugas->getNasabah();

		$this->load->view('components_petugas/header');
		$this->load->view('components_petugas/sidebar');
		$this->load->view('petugas/nasabah',$data);
		$this->load->view('components_petugas/footer');
	}

	public function insertNasabah()
	{
		$this->model_petugas->insertNasabah();
		redirect('index.php/petugas/getNasabah');
	}
	
	public function showNasabah($id)
	{
		$q = $this->db->get_where('tb_nasabah',array('id' => $id))->result_array();
		$q = json_encode($q);
		print_r($q); die;
        return $q;
	}

	public function updateNasabah()
	{
		$id = $this->input->post('id_edit');

		$data = array(
			'status' => $this->input->post('status_edit')
		);

		$this->model_petugas->updateNasabah($id,$data);

		redirect('index.php/petugas/getNasabah');
	}
	
	public function deleteNasabah($id)
	{
		$this->model_petugas->deleteNasabah($id);
		redirect('index.php/petugas/getNasabah');
	}

	public function tukarUang()
	{
		$id = $this->input->post('id_edit');
		// print_r($id); die;
		$this->model_petugas->tukarUang($id);
		redirect('index.php/petugas/getNasabah');
	}

}
