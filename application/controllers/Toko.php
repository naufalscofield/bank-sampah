<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_nasabah');
		$this->load->library('cart');
	}

	public function index()
	{
		$data['barang'] = $this->model_nasabah->getBarang(); 
		$this->load->view('toko/header');
		$this->load->view('toko/index',$data);
		$this->load->view('toko/footer');
	}

	public function addToCart()
	{
		$qty = $this->input->post('qty'); 
		$price = $this->input->post('harga');
		$id_barang = $this->input->post('id_barang');
		
		$stok = $this->db->get_where('tb_barang',array('id' => $id_barang))->row();
		$stokFix = $stok->stok;
		// print_r($stokFix); die;
		if($this->session->userdata('status_login') == 'login'){
			$data = array(
				'id' => $this->input->post('id_barang'),
				'name' => $this->input->post('nama_barang'),
				'qty' => $this->input->post('qty'),
				'price' => $this->input->post('harga'),
				'stok' => $stokFix
			);
			// print_r($data); die;

			$this->cart->insert($data);
			echo"<script>alert('Barang berhasil ditambahkan ke keranjang'); window.location = '../toko/keranjang'</script>";
			} else {
				echo"<script>alert('Harap login terlebih dahulu!'); window.location = '../login'</script>";
			}
	}

	public function keranjang()
	{ 
		if($this->session->userdata('status_login') == 'login'){
			$this->load->view('toko/header');
			$this->load->view('toko/keranjang');
			$this->load->view('toko/footer');
		} else {
			echo"<script>alert('Harap login terlebih dahulu!'); window.location = '../login'</script>";
		}
	}

	public function hapuscart($rowid)
	{
		$data = array(
			'rowid' => $rowid,
			'qty' => 0
		);
		
		$this->cart->update($data);
		echo"<script>alert('Barang dikeluarkan dari keranjang!'); window.location = '../keranjang'</script>";
	}

	public function kurangiItem()
	{
		$qty = $this->input->post('qty');
		$kurang = $this->input->post('kurang');
		// print_r($kurang); die;
		$qtyFix = $qty-$kurang;

		$data = array(
			'rowid' => $this->input->post('rowid'),
			'qty' => $qtyFix
		);
		
		$this->cart->update($data);
		redirect('index.php/toko/keranjang');
	}

	public function checkout($over,$total)
	{
		if($over == '0'){
			
			
			$id_nasabah = $this->session->userdata('id');
			
			$this->model_nasabah->insertCheckout($total,$id_nasabah);
			
			$saldoAwal = $this->db->get_where('tb_nasabah',array('id' => $id_nasabah))->row();
			$saldoAwalFix = $saldoAwal->saldo;
			// print_r($saldoAwalFix); die;
			$saldoAkhir = $saldoAwalFix - $total;
			
			$this->model_nasabah->updateSaldo($saldoAkhir,$id_nasabah);
			
			foreach ($this->cart->contents() as $key) {
				
				$q = $this->db->query("SELECT max(id_transaksi_barang) as id_transaksi from tb_transaksi_barang")->row();
				$id_transaksi = $q->id_transaksi;
				
				$total_harga = $key['qty'] * $key['price'];
				$data=array(
					'id_transaksi_barang' => $id_transaksi,
					'id_barang' => $key['id'],
					'jumlah' => $key['qty'],
					'total_harga' => $total_harga,
				);
				$id_barang = $key['id'];
				$qtyAwal = $this->db->get_where('tb_barang',array('id' => $id_barang))->row();
				$qtyAwalFix = $qtyAwal->stok;
				$qty = $key['qty'];
				$qtyAkhir = $qtyAwalFix - $qty;
			$this->model_nasabah->updateStok($id_barang, $qtyAkhir);
			$this->model_nasabah->insertToTransaksiDetail($data);
		}
		$this->cart->destroy();
		echo"<script>alert('Transaksi Sukses!'); window.location = '../..'</script>";

		}else{
			echo"<script>alert('Ada barang yang melebihi stok! Transaksi dibatalkan'); window.location = '../../keranjang'</script>";
		}
	}

	public function detailBarang($id)
	{
		$this->db->join('tb_kategori','tb_kategori.id = tb_barang.id_kategori');
		$this->db->select('*,tb_barang.id as id_barang');
		$data['barang'] = $this->db->get_where('tb_barang',array('tb_barang.id' => $id))->result_array();
		$this->load->view('toko/header',$data);
		$this->load->view('toko/detail',$data);
		$this->load->view('toko/footer',$data);
	}
	
	public function detailBarangPoin($id)
	{
		$this->db->join('tb_kategori','tb_kategori.id = tb_barang.id_kategori');
		$this->db->select('*,tb_barang.id as id_barang');
		$data['barang'] = $this->db->get_where('tb_barang',array('tb_barang.id' => $id))->result_array();
		$this->load->view('toko/header',$data);
		$this->load->view('toko/detail_poin',$data);
		$this->load->view('toko/footer',$data);
	}
	
	public function  riwayatBelanja()
	{
		$id_nasabah = $this->session->userdata('id');
		$data['riwayat'] = $this->model_nasabah->getRiwayatBelanja($id_nasabah);
		$this->load->view('toko/header');
		$this->load->view('toko/riwayat_belanja',$data);
		$this->load->view('toko/footer');
	}
	
	public function poin()
	{
		$data['poin'] = $this->model_nasabah->getBarangPoin();
		$this->load->view('toko/header');
		$this->load->view('toko/tukarPoin',$data);
		$this->load->view('toko/footer');
	}

	public function aksiTukarPoin()
	{
		$id_barang = $this->input->post('id_barang');
		$stok = $this->input->post('stok');
		$poin = $this->input->post('poin');
		$id_nasabah = $this->session->userdata('id');
		$poinSebelum = $this->db->get_where('tb_nasabah',array('id' => $id_nasabah))->row();
		$poinSebelumFix = $poinSebelum->point;
		$poinSesudah = $poinSebelumFix - $poin;

		$this->model_nasabah->updatePoin($id_nasabah,$poinSesudah);
		$this->model_nasabah->updateStokPoin($id_barang);
		$this->model_nasabah->TransaksiPoin($id_nasabah,$id_barang);

		echo"<script>alert('Penukaran poin sukses! terimakasih telah menggunakan Bank Sampah'); window.location = '../../toko'</script>";
	}

	public function riwayatTukarPoin()
	{
		$id_nasabah = $this->session->userdata('id');
		$data['riwayat'] = $this->model_nasabah->getRiwayatTukarPoin($id_nasabah);
		$this->load->view('toko/header');
		$this->load->view('toko/riwayat_tukar_poin',$data);
		$this->load->view('toko/footer');
	}
}
