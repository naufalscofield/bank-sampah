<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_nasabah');
	}
	public function index()
	{
		$this->load->view('home/header');
		$this->load->view('home/index');
		$this->load->view('home/footer');
	}
	
	public function daftar()
	{
		$data['sektor'] = $this->model_nasabah->getSektor();
		$this->load->view('home/daftar',$data);
	}

	public function aksi_daftar()
	{
		$config['upload_path'] = './assets/nasabah/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('ktp')) {
			$error = $this->upload->display_errors();
			// menampilkan pesan error
			print_r($error);
		} else {
			$result = $this->upload->data();
			$name = $result['file_name'];

			$this->model_nasabah->insertNasabah($name);
			echo"<script>alert('Anda telah berhasil mendaftar!'); window.location = '../welcome'</script>";
		}
	}
	
	public function request()
	{
		if($this->session->userdata('status_login') == 'login'){
			$this->load->view('home/request');
		}else{
			echo"<script>alert('Harap login terlebih dahulu!'); window.location = '../login'</script>";
		}
	}
	
	public function aksi_request()
	{
		$this->model_nasabah->insertRequest();
		echo"<script>alert('Request telah dikirimkan! Silahkan tunggu petugas kami datang'); window.location = '../welcome'</script>";
	}

	public function status_request()
	{
		if($this->session->userdata('status_login') == 'login'){
			$data['request'] = $this->model_nasabah->getRequest();
			$this->load->view('home/status_request',$data);
		}else{
			echo"<script>alert('Harap login terlebih dahulu!'); window.location = '../login'</script>";
		}
	}

	public function batal_request($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('tb_request');
		echo"<script>alert('Request dibatalkan!'); window.location = '../welcome'</script>";
	}

	public function profil()
	{
		$data['saldo'] = $this->model_nasabah->getSaldoPoint();
		$data['point'] = $this->model_nasabah->getSaldoPoint();
		$data['sektor'] = $this->model_nasabah->getSektor();
		$this->load->view('home/profil',$data);
	}

	public function update_profil()
	{
		$this->model_nasabah->updateProfil();
		echo"<script>alert('Profil berhasil diperbaharui! Silahkan login kemabali untuk verifikasi'); window.location = '../login/logout_nasabah'</script>";
	}

}
