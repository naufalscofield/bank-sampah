<?php
  class model_login extends ci_model{

    public function __construct()
    {
        parent::__construct();
    }

    public function cek_login($data)
    {
		return $this->db->get_where('tb_user',$data);
    }
    
    public function cek_login_nasabah($data)
    {
		return $this->db->get_where('tb_nasabah',$data);
    }
    
}
