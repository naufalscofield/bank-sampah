<?php
  class Model_admin extends ci_model{

    public function __construct()
    {
        parent::__construct();
    }

    public function getSektor()
    {
        return $this->db->get('tb_sektor')->result_array();
    }

    //-------------------------------------------------------------------------------------------------------//

    public function updateProfil()
    {
        $id = $this->input->post('id_user');

        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'no_telp' => $this->input->post('no_telp'),
            'alamat' => $this->input->post('alamat'),
            'id_sektor' => $this->input->post('id_sektor'),
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_user',$data);
    }

    public function changeFotoProfil($name)
    {
        $id = $this->input->post('id_user');

        $data = array(
            'foto' => $name
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_user',$data);
    }

    //---------------------------------------------------------------------------------------------------------------//

    public function getPegawai()
    {
        $this->db->join('tb_sektor','tb_sektor.id = tb_user.id_sektor');
        $this->db->select('tb_sektor.sektor, tb_user.id, tb_user.nama_lengkap, tb_user.username, tb_user.no_telp, tb_user.password, tb_user.alamat, tb_user.jabatan, tb_user.foto, tb_user.status');
        $q = $this->db->get('tb_user');
        return $q->result_array();
    }

    public function getPegawaiKosong()
    {
        $q = $this->db->get_where('tb_user',array('jabatan' => 'petugas'));
        return $q->result_array();
    }

    public function insertPegawai($name)
    {
        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('username')),
            'no_telp' => $this->input->post('no_telp'),
            'alamat' => $this->input->post('alamat'),
            'jabatan' => $this->input->post('jabatan'),
            'id_sektor' => $this->input->post('id_sektor'),
            'foto' => $name,
            'status' => 'aktif'
        );

        $this->db->insert('tb_user', $data);
    }

    public function showPegawai($id)
    {
        $q = $this->db->get_where('tb_user',array('id' => $id))->row();
        return json_encode($q);
    }

    public function updatePegawai($id,$data)
    {
        $this->db->where('id',$id);
        return $this->db->update('tb_user',$data);
    }

    public function deletePegawai($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_user');
    }

    //----------------------------------------------------------------------------------------------------------------//

    public function getBarang()
    {
        $this->db->join('tb_kategori', 'tb_kategori.id = tb_barang.id_kategori');
        $this->db->select('tb_barang.id as id, tb_barang.poin as poin, tb_barang.nama_barang, tb_barang.id_kategori, tb_barang.deskripsi, tb_barang.harga, tb_barang.jenis, tb_barang.stok, tb_barang.foto, tb_kategori.nama_kategori');
        $q = $this->db->get('tb_barang');
        return $q->result_array();
    }

    public function insertBarang($name)
    {
        $data = array(
            'nama_barang' => $this->input->post('nama_barang'),
            'id_kategori' => $this->input->post('id_kategori'),
            'deskripsi' => $this->input->post('deskripsi'),
            'harga' => $this->input->post('harga'),
            'poin' => $this->input->post('poin'),
            'stok' => $this->input->post('stok'),
            'jenis' => $this->input->post('jenis'),
            'foto' => $name
        );

        return $this->db->insert('tb_barang',$data);
    }

    public function updateBarang($id)
    {
        $data = array(
            'nama_barang' => $this->input->post('nama_barang_edit'),
            'id_kategori' => $this->input->post('id_kategori_edit'),
            'deskripsi' => $this->input->post('deskripsi_edit'),
            'harga' => $this->input->post('harga_edit'),
            'poin' => $this->input->post('poin_edit'),
            'stok' => $this->input->post('stok_edit'),
            'jenis' => $this->input->post('jenis_edit')
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_barang',$data);
    }

    public function updateBarangG($id, $name)
    {
        $data = array(
            'nama_barang' => $this->input->post('nama_barang_edit'),
            'id_kategori' => $this->input->post('id_kategori_edit'),
            'deskripsi' => $this->input->post('deskripsi_edit'),
            'harga' => $this->input->post('harga_edit'),
            'stok' => $this->input->post('stok_edit'),
            'jenis' => $this->input->post('jenis_edit'),
            'foto' => $name
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_barang',$data);
    }

    public function deleteBarang($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_barang');
    }

    //-----------------------------------------------------------------------------------------------------------------//

    public function getKategori()
    {
        $q = $this->db->get('tb_kategori');
        return $q->result_array();
    }

    public function insertKategori()
    {
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori')
        );

        return $this->db->insert('tb_kategori',$data);
    }

    public function updateKategori($id)
    {
        $data = array(
            'nama_kategori' => $this->input->post('nama_kategori_edit')
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_kategori',$data);
    }

    public function deleteKategori($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_kategori');
    }

    //-----------------------------------------------------------------------------------------------------------------//

    public function getJenisSampah()
    {
        $q = $this->db->get('tb_jenis_sampah');
        return $q->result_array();
    }

    public function insertJenisSampah()
    {
        $data = array(
            'jenis_sampah' => $this->input->post('jenis_sampah'),
            'harga' => $this->input->post('harga'),
            'satuan' => $this->input->post('satuan')
        );

        return $this->db->insert('tb_jenis_sampah',$data);
    }

    public function updateJenisSampah($id)
    {
        $data = array(
            'jenis_sampah' => $this->input->post('jenis_sampah_edit'),
            'harga' => $this->input->post('harga_edit'),
            'satuan' => $this->input->post('satuan_edit')
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_jenis_sampah',$data);
    }

    public function deleteJenisSampah($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_jenis_sampah');
    }

    //-----------------------------------------------------------------------------------------------------------------//

    public function getUmkm()
    {
        $q = $this->db->get('tb_umkm');
        return $q->result_array();
    }

    public function insertUmkm()
    {
        $data = array(
            'nama_umkm' => $this->input->post('nama_umkm'),
            'alamat' => $this->input->post('alamat')
        );

        return $this->db->insert('tb_umkm',$data);
    }

    public function updateUmkm($id)
    {
        $data = array(
            'nama_umkm' => $this->input->post('nama_umkm_edit'),
            'alamat' => $this->input->post('alamat_edit')
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_umkm',$data);
    }

    public function deleteUmkm($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_umkm');
    }

    //-----------------------------------------------------------------------------------------------------------------//

    public function getRequest()
    {
        $q = $this->db->query('SELECT tb_request.id as id, tb_nasabah.id as id_nasabah, tb_nasabah.nama_lengkap as nama_nasabah, tb_nasabah.alamat as alamat_nasabah, tb_request.status as status , tb_user.id as id_user, tb_user.nama_lengkap as nama_petugas, tb_request.jenis_request as jenis_request FROM `tb_request` left join tb_user on tb_user.id = tb_request.id_pegawai left join tb_nasabah on tb_nasabah.id = tb_request.id_nasabah');
        return $q->result_array();
    }

    public function getNasabahSektor($id_nasabah)
    {
        $this->db->where('id',$id_nasabah);
        $this->db->select('id_sektor');
        $q = $this->db->get('tb_nasabah');
        return $q->row();
    }

    public function updateRequest($id)
    {
        $data = array(
            'id_pegawai' => $this->input->post('id_petugas_edit'),
            'status' => 'diproses'
        );

        $this->db->where('id',$id);
        return $this->db->update('tb_request',$data);
    }

    public function deleteRequest($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_request');
    }

    //-----------------------------------------------------------------------------------------------------------------//

    public function getTransaksiSampah()
    {
        $this->db->join('tb_nasabah','tb_nasabah.id = tb_transaksi_sampah.id_nasabah');
        $q = $this->db->get('tb_transaksi_sampah');
        return $q->result_array();
    }

    public function getTransaksiSampahDate($date1,$date2)
    {
        $this->db->join('tb_nasabah','tb_nasabah.id = tb_transaksi_sampah.id_nasabah');
        $this->db->where('tanggal >',$date1);
        $this->db->where('tanggal <',$date2);
        $q = $this->db->get('tb_transaksi_sampah');
        return $q->result_array();
    }

    public function getDetailSumbangSampah($id_sumbang_sampah_detail)
    {
        $q = $this->db->query("
        select * from tb_sumbang_sampah_detail where id_sumbang_sampah_detail = $id_sumbang_sampah_detail")->result_array();
        $new = json_encode($q);
        echo $new;
    }

    public function getSumbangSampah()
    {
        $this->db->join('tb_nasabah','tb_nasabah.id = tb_sumbang_sampah_detail.id_nasabah');
        // $this->db->join('tb_sumbang_sampah_detail','tb_sumbang_sampah_detail.id_sumbang_sampah = tb_sumbang_sampah.id_sumbang_sampah');
        $q = $this->db->get('tb_sumbang_sampah_detail');
        return $q->result_array();
    }

    // public function updateHibah($where, $data, $table)
    // {
    //     $this->db->where($where);
    //     return $this->db->update($table,$data);
    // }

    public function deleteTransaksiSampah($id)
    {
        $this->db->where('id_transaksi_sampah',$id);
        $this->db->delete('tb_transaksi_sampah');
    }
    public function deleteTransaksiSampah2($id)
    {
        $this->db->where('id_transaksi_sampah',$id);
        $this->db->delete('tb_transaksi_sampah_detail');
    }

    //-----------------------------------------------------------------------------------------------------------------//

    public function getTransaksiBarang()
    {
        $this->db->join('tb_nasabah','tb_nasabah.id = tb_transaksi_barang.id_nasabah');
        $q = $this->db->get('tb_transaksi_barang');
        return $q->result_array();
    }

    public function deleteTransaksiBarang($id)
    {
        $this->db->where('id_transaksi_Barang',$id);
        $this->db->delete('tb_transaksi_Barang');
    }

    public function deleteTransaksiBarang2($id)
    {
        $this->db->where('id_transaksi_Barang',$id);
        $this->db->delete('tb_transaksi_Barang_detail');
    }

    //---------------------------------------------------------------------------------------------------------------//

    public function getNasabah()
    {
        $this->db->join('tb_sektor','tb_sektor.id = tb_nasabah.id_sektor','left');
        $this->db->select('*,tb_sektor.id as id_sektor, tb_nasabah.id as id_nasabah');
        $q = $this->db->get('tb_nasabah');
        return $q->result_array();
    }

    public function insertNasabah()
    {
        $random1 = mt_rand(000,999);
        $random2 = mt_rand(000,999);
        $random3 = mt_rand(000,999);
        $random4 = mt_rand(000,999);
        $rek = "$random1$random2$random3$random4";
        // print_r($rek); die;
        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'no_rekening' => $rek,
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('username')),
            'no_telp' => $this->input->post('no_telp'),
            'alamat' => $this->input->post('alamat'),
            'id_sektor' => $this->input->post('id_sektor'),
            'saldo' => '',
            'point' => '',
            'status' => 'aktif'
        );

        $this->db->insert('tb_nasabah', $data);
    }

    public function updateNasabah($id,$data)
    {
        $this->db->where('id',$id);
        return $this->db->update('tb_nasabah',$data);
    }

    public function tukarUang($id)
    {
        $saldoAwal = $this->input->post('saldo_edit');
        $nominal = $this->input->post('nominal_edit');
        $saldoAkhir = $saldoAwal - $nominal;
        // print_r($saldoAkhir); die;
        $data = array(
            'saldo' => $saldoAkhir
        );
        $this->db->where('id',$id);
        return $this->db->update('tb_nasabah',$data);
    }

    public function deleteNasabah($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tb_nasabah');
    }

    public function getPendapatan()
    {
        $q = $this->db->get('tb_pendapatan');
        return $q->result_array();
    }

    public function getPenjualan()
    {
        $this->db->join('tb_jenis_sampah','tb_jenis_sampah.id = tb_penjualan.id_jenis_sampah');
        $q = $this->db->get('tb_penjualan');
        return $q->result_array();
    }

    public function getHargaSampah($id_jenis_sampah)
    {
        $q = $this->db->query("
        select * from tb_jenis_sampah where id = $id_jenis_sampah")->result_array();
        $new = json_encode($q);
        echo $new;
    }

    public function insertPenjualan()
    {
        $berat = $this->input->post('berat');
        $harga = $this->input->post('harga');
        $total = $berat * $harga;

        $data = array(
            'pembeli' => $this->input->post('pembeli'),
            'id_jenis_sampah' => $this->input->post('id_jenis_sampah'),
            'berat' => $berat,
            'total' => $total
        );

        return $this->db->insert('tb_penjualan',$data);
    }

    public function insertPendapatan()
    {
        $date = date('Y-m-d');
        $berat = $this->input->post('berat');
        $harga = $this->input->post('harga');
        $total = $berat * $harga;

        $this->db->order_by('id', 'DESC');
        $saldo = $this->db->get('tb_pendapatan')->row();
        $saldoFix = $saldo->saldo;

        $saldoAkhir = $saldoFix + $total;

        $tgl=date('Y-m-d');
        $data = array (
            'tanggal' => $tgl,
            'pemasukan' => $total,
            'pengeluaran' => null,
            'saldo' => $saldoAkhir
        );

        return $this->db->insert('tb_pendapatan',$data);
    }
}
