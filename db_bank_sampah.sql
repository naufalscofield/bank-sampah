-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2019 at 05:18 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bank_sampah`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `poin` int(11) DEFAULT NULL,
  `stok` int(11) NOT NULL,
  `foto` text NOT NULL,
  `jenis` enum('regular','eksklusif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_barang`
--

INSERT INTO `tb_barang` (`id`, `nama_barang`, `id_kategori`, `deskripsi`, `harga`, `poin`, `stok`, `foto`, `jenis`) VALUES
(1, 'Malkist Roma', 1, 'Biskut Malkist merk Roma. Enak Untuk cemilan sehari-hari. Terbuat dari bahan berkualitas', 5000, NULL, 7, '85d106b7b97e04237ba41344cd710bf3.jpg', 'regular'),
(2, 'Super Sol', 3, 'Super Sol. Pewangi dan pembersih lantai. Lantai bebas dari kuman.', 13500, NULL, 10, 'super-sol-karbol-wangi-lemon-ref-1-8l-pcs.jpg', 'regular'),
(3, 'Tempat Sampah', 3, 'Tempat Sampah Tutup Goyang Komet Star. Kapasitas 20L', 75000, NULL, 5, '3581953_f783b7f9-52c2-4b4f-a15b-55d33b2d2b8b_700_700.jpg', 'regular'),
(5, 'Emas', 6, 'Emas Antam Murni 1 gram', NULL, 100, 5, 'emas1.jpg', 'eksklusif'),
(6, 'Bebelac 3', 1, 'Susu Anak umur 1-3 Tahun merk Bebelac. Mengandung banyak minyak ikan', 60000, NULL, 10, 'bebelac3-bebenutri-plus-susu-pertumbuhan-vanila-400gr-5760-65033331-bd8224a269f9763487a3b48ae767418b-catalog_233.jpg', 'regular');

-- --------------------------------------------------------

--
-- Table structure for table `tb_hibah`
--

CREATE TABLE `tb_hibah` (
  `id` int(11) NOT NULL,
  `id_perusahaan` int(11) NOT NULL,
  `id_umkm` int(11) NOT NULL,
  `jenis_sampah` varchar(20) NOT NULL,
  `berat` int(11) NOT NULL,
  `satuan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_hibah`
--

INSERT INTO `tb_hibah` (`id`, `id_perusahaan`, `id_umkm`, `jenis_sampah`, `berat`, `satuan`) VALUES
(1, 7, 1, 'Jirigen', 5, 'item'),
(2, 7, 1, 'Jirigen', 1, 'item'),
(3, 10, 2, 'Tong Plastik', 2, 'item'),
(4, 5, 2, 'Tong', 3, 'item');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_sampah`
--

CREATE TABLE `tb_jenis_sampah` (
  `id` int(11) NOT NULL,
  `jenis_sampah` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_sampah`
--

INSERT INTO `tb_jenis_sampah` (`id`, `jenis_sampah`, `harga`, `satuan`) VALUES
(1, 'PP Air Mineral', 5100, 'kg'),
(2, 'PET Botol Bening', 3500, 'kg'),
(3, 'Tutup Air Galon', 4300, 'kg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `nama_kategori`) VALUES
(1, 'Makanan & Minuman'),
(2, 'Sembako'),
(3, 'Alat Kebersihan'),
(6, 'Perhiasan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nasabah`
--

CREATE TABLE `tb_nasabah` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `no_rekening` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `id_sektor` int(11) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `saldo` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `jenis_nasabah` enum('individual','organisasi','perusahaan') NOT NULL,
  `ktp` text NOT NULL,
  `status` enum('aktif','tidak_aktif','pending') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nasabah`
--

INSERT INTO `tb_nasabah` (`id`, `nama_lengkap`, `no_rekening`, `alamat`, `id_sektor`, `username`, `password`, `no_telp`, `saldo`, `point`, `jenis_nasabah`, `ktp`, `status`) VALUES
(1, 'Visca', '2147480000012', 'polposaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 'viscanasabah', '19bdc9e76ce067fa741d0c09267ecf3d', '081111111234', 18120, 0, 'individual', 'ktp.jpg', 'aktif'),
(2, 'eko', '606472340676', 'suk', 3, 'ekonasabah', '11ccf8f82540766c5cb01a628244246e', '081111111234', 35120, 0, 'individual', 'ktp.jpg', 'aktif'),
(3, 'Nizar', '995592944922', 'Jln.Sukarasa', 2, 'nizarnasabah', 'c2fe18f76935ca70dfa370793e129e03', '081111111234', 0, 0, 'individual', 'ktp.jpg', 'aktif'),
(4, 'RT09', '486211605392', 'Jln.bunga', 6, 'rt09nasabah', '28b2291b24e030221b729933fab886b9', '81234567889', 744000, 5, 'organisasi', 'ktp.jpg', 'aktif'),
(5, 'PT Wika', '263145890611', 'Bogor Cileungsi', 0, 'wikanasabah', '3222e53dbe52b884d93441dbf2f84e65', '089012345678', 0, 0, 'perusahaan', 'ktp.jpg', 'aktif'),
(6, 'PT Pos', '428434496795', 'Jln Banda', 0, 'posnasabah', '1caeb838b399b4ddfbcc5185e8fc1f0a', '081111111234', 0, 0, 'perusahaan', 'ktp.jpg', 'aktif'),
(7, 'PT Ogah Rugi', '389269214140', 'Jl. Anggerk 2, Ds. Cimareme, Kec. Ngamprah', 0, 'ogahrugi', '289bce3f746ab14d00396574ae645331', '08789098776', 0, 0, 'perusahaan', 'ktp.jpg', 'aktif'),
(8, 'Jessica Jones', '87978480749', 'Jl. Wash Burn No.136 Ds. Cimareme, Kec. Ngamprah', 4, 'jessicanasabah', '78e1580f23a27edb1494d1fcdf874e0b', '081322868967', 48300, 2, 'individual', 'ktp.jpg', 'aktif'),
(9, 'Randika saputra', '24427340307', 'Kp.pangsor desa ngamparah kecamatan ngamprah', 7, 'Randika', '6a9c1a76f3713e27fd86b95d03b246ed', '085910989035', 12500, 0, 'individual', 'ktp.jpg', 'aktif'),
(10, 'PT. iCool indonesia', '58022427550', 'Jl. Cihampelas No 214 A', 0, 'icool2019', '6937f97dcb754f5dabaead5a7eb3839c', '08199775544', 0, 0, 'perusahaan', 'ktp.jpg', 'aktif'),
(11, 'Ridwan', '83254484883', 'Jl. Anggrek 2 n0 32 ds. cimareme, kec. ngamprah', 4, 'ridwan', 'd584c96e6c1ba3ca448426f66e552e8e', '0832245678', 18800, 0, 'individual', 'ktp.jpg', 'aktif'),
(12, 'jon', '213919648974', 'winterfell', 1, 'jonnasabah', 'b92e13d3aef654679fd1dec96a3055ba', '081111111234', 17200, 0, 'individual', 'ktp.jpg', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pendapatan`
--

CREATE TABLE `tb_pendapatan` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `pemasukan` int(11) DEFAULT NULL,
  `pengeluaran` int(11) DEFAULT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pendapatan`
--

INSERT INTO `tb_pendapatan` (`id`, `tanggal`, `pemasukan`, `pengeluaran`, `saldo`) VALUES
(1, '2019-08-31', 1000000, NULL, 1000000),
(3, '2019-08-31', NULL, 70000, 930000),
(4, '2019-09-01', NULL, 7000, 923000),
(5, '2019-09-01', 28050, NULL, 951050),
(6, '2019-09-01', 7700, NULL, 958750),
(7, '2019-09-01', 47300, NULL, 1006050),
(8, '2019-09-01', 37840, NULL, 1043890),
(9, '2019-09-01', 33110, NULL, 1077000),
(10, '2019-09-01', 56100, NULL, 1133100),
(11, '2019-09-01', 30800, NULL, 1163900);

-- --------------------------------------------------------

--
-- Table structure for table `tb_penjualan`
--

CREATE TABLE `tb_penjualan` (
  `id` int(11) NOT NULL,
  `pembeli` varchar(50) NOT NULL,
  `id_jenis_sampah` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penjualan`
--

INSERT INTO `tb_penjualan` (`id`, `pembeli`, `id_jenis_sampah`, `berat`, `total`) VALUES
(1, 'surabi', 2, 2, 7700),
(2, 'a', 3, 2, 47300),
(3, 'sss', 3, 1, 37840),
(4, 'huu', 3, 1, 33110),
(5, 'bebe', 1, 1, 56100),
(6, 'huk', 2, 1, 30800);

-- --------------------------------------------------------

--
-- Table structure for table `tb_request`
--

CREATE TABLE `tb_request` (
  `id` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `jenis_request` enum('penukaran sampah','penukaran uang','sumbang sampah') NOT NULL,
  `tanggal` date NOT NULL,
  `ukuran_sampah` enum('regular','besar','jumbo') NOT NULL,
  `tanggal_diambil` date NOT NULL,
  `deskripsi` text NOT NULL,
  `status` enum('pending','diproses','menuju','selesai') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_request`
--

INSERT INTO `tb_request` (`id`, `id_nasabah`, `id_pegawai`, `jenis_request`, `tanggal`, `ukuran_sampah`, `tanggal_diambil`, `deskripsi`, `status`) VALUES
(1, 8, 6, 'penukaran sampah', '2019-08-20', 'regular', '0000-00-00', '', 'selesai'),
(2, 7, 5, 'sumbang sampah', '2019-08-20', 'regular', '0000-00-00', '', 'selesai'),
(3, 8, 6, 'penukaran sampah', '2019-08-25', 'regular', '0000-00-00', '', 'selesai'),
(4, 9, 7, 'penukaran sampah', '2019-08-25', 'regular', '0000-00-00', '', 'selesai'),
(5, 10, 5, 'sumbang sampah', '2019-08-25', 'regular', '0000-00-00', '', 'selesai'),
(6, 8, 6, 'penukaran sampah', '2019-08-26', 'regular', '0000-00-00', '', 'menuju'),
(7, 6, 5, 'sumbang sampah', '2019-08-26', 'regular', '0000-00-00', '', 'selesai'),
(8, 5, 7, 'sumbang sampah', '2019-08-26', 'regular', '0000-00-00', '', 'selesai'),
(9, 11, 6, 'penukaran sampah', '2019-08-27', 'regular', '0000-00-00', '', 'selesai'),
(10, 12, 2, 'penukaran sampah', '2019-08-29', 'regular', '2019-09-02', 'botol botol', 'selesai');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sektor`
--

CREATE TABLE `tb_sektor` (
  `id` int(11) NOT NULL,
  `sektor` varchar(20) NOT NULL,
  `hari_ambil` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sektor`
--

INSERT INTO `tb_sektor` (`id`, `sektor`, `hari_ambil`) VALUES
(1, 'bojongkoneng', 'monday'),
(2, 'cilame', 'monday'),
(3, 'cimanggu', 'tuesday'),
(4, 'cimareme', 'tuesday'),
(5, 'gadobangkong', 'wednesday'),
(6, 'margajaya', 'wednesday'),
(7, 'ngamprah', 'thursday'),
(8, 'mekarsari', 'thursday'),
(9, 'pakuhaji', 'friday'),
(10, 'sukatani', 'friday'),
(11, 'tanimulya', 'saturday');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sumbang_sampah`
--

CREATE TABLE `tb_sumbang_sampah` (
  `id_sumbang_sampah` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sumbang_sampah`
--

INSERT INTO `tb_sumbang_sampah` (`id_sumbang_sampah`, `id_nasabah`, `tanggal`) VALUES
(1, 7, '2019-08-20'),
(2, 10, '2019-08-26'),
(3, 6, '2019-08-26'),
(4, 5, '2019-08-26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sumbang_sampah_detail`
--

CREATE TABLE `tb_sumbang_sampah_detail` (
  `id_sumbang_sampah_detail` int(11) NOT NULL,
  `id_sumbang_sampah` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jenis_sampah` varchar(20) NOT NULL,
  `berat` int(11) NOT NULL,
  `berat2` int(11) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sumbang_sampah_detail`
--

INSERT INTO `tb_sumbang_sampah_detail` (`id_sumbang_sampah_detail`, `id_sumbang_sampah`, `id_nasabah`, `tanggal`, `jenis_sampah`, `berat`, `berat2`, `satuan`, `keterangan`) VALUES
(1, 1, 7, '2019-08-20', 'Jirigen', 3, 0, 'item', 'Untuk disumbangkan ke UMKM'),
(2, 2, 10, '2019-08-26', 'Tong Plastik', 8, 0, 'item', 'Sumbangan dari PT. ICool untuk umkm di kec. ngamprah'),
(3, 3, 6, '2019-08-26', 'Jirigen', 10, 0, 'item', 'Untuk UMKM Di Kec. Ngamprah'),
(4, 4, 5, '2019-08-26', 'Tong', 5, 8, 'item', 'Untuk dikelola oleh Bank Sampah Kreatif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_barang`
--

CREATE TABLE `tb_transaksi_barang` (
  `id_transaksi_barang` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_biaya` int(11) NOT NULL,
  `status_pembayaran` enum('lunas','belum_lunas','','') NOT NULL,
  `status_pengantaran` enum('pending','diantar','selesai','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_barang`
--

INSERT INTO `tb_transaksi_barang` (`id_transaksi_barang`, `id_nasabah`, `tanggal`, `total_biaya`, `status_pembayaran`, `status_pengantaran`) VALUES
(1, 9, '2019-08-25', 5000, 'lunas', 'selesai'),
(2, 8, '2019-08-27', 5000, 'lunas', 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_barang_detail`
--

CREATE TABLE `tb_transaksi_barang_detail` (
  `id_transaksi_barang_detail` int(11) NOT NULL,
  `id_transaksi_barang` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_barang_detail`
--

INSERT INTO `tb_transaksi_barang_detail` (`id_transaksi_barang_detail`, `id_transaksi_barang`, `id_barang`, `jumlah`, `total_harga`) VALUES
(1, 1, 1, 1, 5000),
(2, 2, 1, 1, 5000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_poin`
--

CREATE TABLE `tb_transaksi_poin` (
  `id` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_sampah`
--

CREATE TABLE `tb_transaksi_sampah` (
  `id_transaksi_sampah` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_sampah`
--

INSERT INTO `tb_transaksi_sampah` (`id_transaksi_sampah`, `id_nasabah`, `tanggal`, `total_harga`) VALUES
(1, 8, '2019-08-20', 11300),
(2, 8, '2019-08-25', 47000),
(3, 9, '2019-08-25', 17500),
(4, 11, '2019-08-27', 18800),
(5, 12, '2019-09-01', 10200),
(6, 12, '2019-09-01', 7000),
(7, 12, '2019-09-01', 7000),
(8, 12, '2019-09-01', 7000),
(9, 12, '2019-09-01', 7000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_sampah_detail`
--

CREATE TABLE `tb_transaksi_sampah_detail` (
  `id_transaksi_sampah_detail` int(11) NOT NULL,
  `id_transaksi_sampah` int(11) NOT NULL,
  `id_jenis_sampah` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_sampah_detail`
--

INSERT INTO `tb_transaksi_sampah_detail` (`id_transaksi_sampah_detail`, `id_transaksi_sampah`, `id_jenis_sampah`, `berat`, `harga`) VALUES
(1, 1, 2, 2, 3500),
(2, 1, 3, 1, 4300),
(3, 2, 1, 5, 5100),
(4, 2, 3, 5, 4300),
(5, 3, 2, 5, 3500),
(6, 4, 1, 2, 5100),
(7, 4, 3, 2, 4300),
(8, 5, 1, 2, 5100),
(9, 9, 2, 2, 3500);

-- --------------------------------------------------------

--
-- Table structure for table `tb_umkm`
--

CREATE TABLE `tb_umkm` (
  `id` int(11) NOT NULL,
  `nama_umkm` varchar(20) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_umkm`
--

INSERT INTO `tb_umkm` (`id`, `nama_umkm`, `alamat`) VALUES
(1, 'UMKM Mandiri', 'Jln. Sukarasa'),
(2, 'UMKM Sejahtera', 'Jl. Sukarasa, Ds. Cimareme, Kec. Ngamparh');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` enum('admin','petugas') NOT NULL,
  `foto` text NOT NULL,
  `status` enum('aktif','tidak_aktif') NOT NULL,
  `id_sektor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama_lengkap`, `username`, `password`, `no_telp`, `alamat`, `jabatan`, `foto`, `status`, `id_sektor`) VALUES
(1, 'Miqo Mahardika Aji', 'miqoadmin', 'df55868cb5f494496784a95e96351d74', '081234567899', 'Jln.Ciparay Raya Mantul', 'admin', '5530192-0023592409-tumbl.jpg', 'aktif', 10),
(2, 'Naufal Ramadhan Ku', 'naufalpetugas', '81c402ddbc018026551df68f526ea429', '081111111235', 'Jlnn.Sukarasa Leghorn58', 'petugas', '292789.jpg', 'aktif', 1),
(4, 'Budi Sudarsono', 'budipetugas', 'c3ff337566eab10b6f73717fe58e0ae2', '081234567890', 'Jln.kebayoran', 'petugas', 'jpinkman.jpg', 'aktif', 2),
(5, 'abduloh', 'abdulpetugas', 'f0dc1cd526d546ff13fbc33003b155f2', '089811111', 'polpos mania', 'petugas', 'frans1.jpg', 'aktif', 3),
(6, 'Ariana Grandee', 'arianapetugas1', 'e7b11aab31270b59a98c68a0626bb08f', '0812345', 'Amerkaa', 'petugas', 'asean.png', 'aktif', 4),
(7, 'Kevin De Bruyne', 'kevinpetugas', 'd8112d9ec097ec25e9c82bc3fd92ad0b', '081234567890', 'Jln.Ciheulang', 'petugas', 'asean1.png', 'aktif', 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `tb_hibah`
--
ALTER TABLE `tb_hibah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_jenis_sampah`
--
ALTER TABLE `tb_jenis_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pendapatan`
--
ALTER TABLE `tb_pendapatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_request`
--
ALTER TABLE `tb_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_sektor`
--
ALTER TABLE `tb_sektor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_sumbang_sampah`
--
ALTER TABLE `tb_sumbang_sampah`
  ADD PRIMARY KEY (`id_sumbang_sampah`);

--
-- Indexes for table `tb_sumbang_sampah_detail`
--
ALTER TABLE `tb_sumbang_sampah_detail`
  ADD PRIMARY KEY (`id_sumbang_sampah_detail`);

--
-- Indexes for table `tb_transaksi_barang`
--
ALTER TABLE `tb_transaksi_barang`
  ADD PRIMARY KEY (`id_transaksi_barang`);

--
-- Indexes for table `tb_transaksi_barang_detail`
--
ALTER TABLE `tb_transaksi_barang_detail`
  ADD PRIMARY KEY (`id_transaksi_barang_detail`);

--
-- Indexes for table `tb_transaksi_poin`
--
ALTER TABLE `tb_transaksi_poin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_transaksi_sampah`
--
ALTER TABLE `tb_transaksi_sampah`
  ADD PRIMARY KEY (`id_transaksi_sampah`);

--
-- Indexes for table `tb_transaksi_sampah_detail`
--
ALTER TABLE `tb_transaksi_sampah_detail`
  ADD PRIMARY KEY (`id_transaksi_sampah_detail`);

--
-- Indexes for table `tb_umkm`
--
ALTER TABLE `tb_umkm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_hibah`
--
ALTER TABLE `tb_hibah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_jenis_sampah`
--
ALTER TABLE `tb_jenis_sampah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_pendapatan`
--
ALTER TABLE `tb_pendapatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_penjualan`
--
ALTER TABLE `tb_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_request`
--
ALTER TABLE `tb_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_sektor`
--
ALTER TABLE `tb_sektor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_sumbang_sampah`
--
ALTER TABLE `tb_sumbang_sampah`
  MODIFY `id_sumbang_sampah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_sumbang_sampah_detail`
--
ALTER TABLE `tb_sumbang_sampah_detail`
  MODIFY `id_sumbang_sampah_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_transaksi_barang`
--
ALTER TABLE `tb_transaksi_barang`
  MODIFY `id_transaksi_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_transaksi_barang_detail`
--
ALTER TABLE `tb_transaksi_barang_detail`
  MODIFY `id_transaksi_barang_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_transaksi_poin`
--
ALTER TABLE `tb_transaksi_poin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_transaksi_sampah`
--
ALTER TABLE `tb_transaksi_sampah`
  MODIFY `id_transaksi_sampah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_transaksi_sampah_detail`
--
ALTER TABLE `tb_transaksi_sampah_detail`
  MODIFY `id_transaksi_sampah_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_umkm`
--
ALTER TABLE `tb_umkm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD CONSTRAINT `tb_barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
